<?php
    function send_wa_message_text($phone_number, $message){
        $time = time().'000';
        $SIGNATURE = hash('sha256', DUITKU_MERCHANT_CODE.$time.DUITKU_MERCHANT_KEY);
        $url = ONESENDER_API_URL . 'messages';
        $body = array(
                "recipient_type" => "individual",
                "to" => $phone_number,
                "type" => "text",
                "text" => array(
                    "body" => $message
                )
        );
        $header = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer '.ONESENDER_API_KEY
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_POSTFIELDS => json_encode($body)
        ));

        $response = curl_exec($ch);
        $is_success = 1;
        if (curl_errno($ch)) {
            $response = curl_error($ch);
            $is_success = 0;
        }else{
            $response = json_decode($response, TRUE);
        }
        curl_close($ch);

        return array(
            "is_success" => $is_success,
            "response" => $response
        );
    }

?>