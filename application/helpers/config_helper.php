<?php
  function get_premium_listing_price($subscribe_type){
    $CI = & get_instance();
    $config = $CI->config_model->get_config();
    if($subscribe_type == 'DAILY'){
      return $config->premium_daily_price;
    }else if($subscribe_type == 'WEEKLY'){
      return $config->premium_weekly_price;
    }else if($subscribe_type == 'MONTHLY'){
      return $config->premium_monthly_price;
    }else if($subscribe_type == '3 MONTH'){
      return $config->premium_3month_price;
    }else if($subscribe_type == '6 MONTH'){
      return $config->premium_6month_price;
    }else if($subscribe_type == 'ANNUAL'){
      return $config->premium_annual_price;
    }else{
      return 0;
    }
  }
?>