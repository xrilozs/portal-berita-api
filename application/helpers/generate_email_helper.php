<?php
    function send_registration_email($email, $url){
        $CI =& get_instance();

        $html = $CI->load->view('template/registration_email',[],true);
        $html = str_replace('$url', $url, $html);
        $mail = new Send_mail();
        $emailResp = $mail->send($email, 'Registration Verification', $html);
        return $emailResp;
    }

    function send_forget_password_email($email, $url){
        $CI =& get_instance();

        $html = $CI->load->view('template/forget_password_email',[],true);
        $html = str_replace('$url', $url, $html);

        $mail = new Send_mail();
        $emailResp = $mail->send($email, 'Forget Password', $html);
        return $emailResp;
    }

    function send_website_verification_email($email, $url){
        $CI =& get_instance();
        $web_url =  WEB_URL . "seller/website";

        $html = $CI->load->view('template/website_verification_email',[],true);
        $html = str_replace('$web_url', $web_url, $html);
        $html = str_replace('$url', $url, $html);
        $mail = new Send_mail();
        $emailResp = $mail->send($email, 'Website Verification', $html);
        return $emailResp;
    }

    function send_order_email($email, $url){
        $CI =& get_instance();
        $web_url =  WEB_URL . "seller/order";

        $html = $CI->load->view('template/order_email',[],true);
        $html = str_replace('$web_url', $web_url, $html);
        $html = str_replace('$url', $url, $html);
        $mail = new Send_mail();
        $emailResp = $mail->send($email, 'Pesanan', $html);
        return $emailResp;
    }

    function send_reject_order_email($email, $url, $reject_reason){
        $CI =& get_instance();
        $web_url =  WEB_URL . "buyer/order";

        $html = $CI->load->view('template/reject_order_email',[],true);
        $html = str_replace('$web_url', $web_url, $html);
        $html = str_replace('$url', $url, $html);
        $html = str_replace('$reject_reason', $reject_reason, $html);
        $mail = new Send_mail();
        $emailResp = $mail->send($email, 'Pesanan Ditolak', $html);
        return $emailResp;
    }

    function send_accept_order_email($email, $url){
        $CI =& get_instance();
        $web_url =  WEB_URL . "buyer/order";

        $html = $CI->load->view('template/accept_order_email',[],true);
        $html = str_replace('$web_url', $web_url, $html);
        $html = str_replace('$url', $url, $html);
        $mail = new Send_mail();
        $emailResp = $mail->send($email, 'Pesanan Diterima', $html);
        return $emailResp;
    }

    function send_auto_cancel_order_email($email, $url, $web_url){
        $CI =& get_instance();

        $html = $CI->load->view('template/auto_cancel_order_email',[],true);
        $html = str_replace('$web_url', $web_url, $html);
        $html = str_replace('$url', $url, $html);
        $mail = new Send_mail();
        $emailResp = $mail->send($email, 'Pesanan Dibatalkan', $html);
        return $emailResp;
    }

    function send_withdraw_reject_email($email, $withdraw, $reject_reason){
        $CI =& get_instance();
        $web_url =  WEB_URL . "seller/withdraw";
        $amount = "Rp " . number_format($withdraw->amount);

        $html = $CI->load->view('template/reject_withdraw_email',[],true);
        $html = str_replace('$web_url', $web_url, $html);
        $html = str_replace('$amount', $amount, $html);
        $html = str_replace('$withdraw_number', $withdraw->withdraw_number, $html);
        $html = str_replace('$reject_reason', $reject_reason, $html);
        $mail = new Send_mail();
        $emailResp = $mail->send($email, 'Penarikan Dana', $html);
        return $emailResp;
    }

    function send_withdraw_paid_email($email, $withdraw){
        $CI =& get_instance();
        $web_url =  WEB_URL . "seller/withdraw";
        $amount = "Rp " . number_format($withdraw->amount);

        $html = $CI->load->view('template/paid_withdraw_email',[],true);
        $html = str_replace('$web_url', $web_url, $html);
        $html = str_replace('$amount', $amount, $html);
        $html = str_replace('$withdraw_number', $withdraw->withdraw_number, $html);
        $html = str_replace('$bank_code', $withdraw->bank_code, $html);
        $html = str_replace('$bank_account_number', $withdraw->bank_account_number, $html);
        $mail = new Send_mail();
        $emailResp = $mail->send($email, 'Penarikan Dana', $html);
        return $emailResp;
    }

    function send_delivery_order_email($email, $seller_fullname){
        $CI =& get_instance();
        $web_url =  WEB_URL . "buyer/order";

        $html = $CI->load->view('template/delivery_order_email',[],true);
        $html = str_replace('$web_url', $web_url, $html);
        $html = str_replace('$seller_fullname', $seller_fullname, $html);
        $mail = new Send_mail();
        $emailResp = $mail->send($email, 'Penyerahan Pekerjaan', $html);
        return $emailResp;
    }

    function send_complete_order_email($email, $buyer_fullname, $fee){
        $CI =& get_instance();
        $web_url =  WEB_URL . "seller/order";

        $html = $CI->load->view('template/complete_order_email',[],true);
        $html = str_replace('$web_url', $web_url, $html);
        $html = str_replace('$buyer_fullname', $buyer_fullname, $html);
        $html = str_replace('$fee', $fee, $html);
        $mail = new Send_mail();
        $emailResp = $mail->send($email, 'Pekerjaan Diterima', $html);
        return $emailResp;
    }

    function send_revision_order_email($email, $buyer_fullname){
        $CI =& get_instance();
        $web_url =  WEB_URL . "seller/order";

        $html = $CI->load->view('template/revision_order_email',[],true);
        $html = str_replace('$web_url', $web_url, $html);
        $html = str_replace('$buyer_fullname', $buyer_fullname, $html);
        $mail = new Send_mail();
        $emailResp = $mail->send($email, 'Pekerjaan Diterima', $html);
        return $emailResp;
    }

    function send_cancel_order_email($email, $buyer_fullname, $invoice_number, $cancel_reason){
        $CI =& get_instance();
        $web_url =  WEB_URL . "seller/order";

        $html = $CI->load->view('template/cancel_order_email',[],true);
        $html = str_replace('$web_url', $web_url, $html);
        $html = str_replace('$buyer_fullname', $buyer_fullname, $html);
        $html = str_replace('$invoice_number', $invoice_number, $html);
        $html = str_replace('$cancel_reason', $cancel_reason, $html);
        $mail = new Send_mail();
        $emailResp = $mail->send($email, 'Pekerjaan Dibatalkan', $html);
        return $emailResp;
    }

    function send_reject_cancel_order_email($email, $invoice_number, $reject_reason){
        $CI =& get_instance();
        $web_url =  WEB_URL . "buyer/order";

        $html = $CI->load->view('template/reject_cancel_order_email',[],true);
        $html = str_replace('$web_url', $web_url, $html);
        $html = str_replace('$invoice_number', $invoice_number, $html);
        $html = str_replace('$reject_reason', $reject_reason, $html);
        $mail = new Send_mail();
        $emailResp = $mail->send($email, 'Pekerjaan Tidak Dibatalkan', $html);
        return $emailResp;
    }
?>