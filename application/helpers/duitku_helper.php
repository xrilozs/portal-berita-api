<?php
    function create_deposit($deposit_number, $deposit_amount, $customer_email){
        $time = time().'000';
        $SIGNATURE = hash('sha256', DUITKU_MERCHANT_CODE.$time.DUITKU_MERCHANT_KEY);
        $url = DUITKU_API . '/merchant/createInvoice';
        $body = array(
            'paymentAmount' => intval($deposit_amount),
            'merchantOrderId' => $deposit_number,
            'productDetails' => "Deposit ".$deposit_amount,
            'email' => $customer_email,
            'callbackUrl' => DUITKU_DEPOSIT_CALLBACK_URL,
            'returnUrl' => DUITKU_RETURN_URL
        );
        $header = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'x-duitku-signature: '.$SIGNATURE,
            'x-duitku-timestamp: '.$time,
            'x-duitku-merchantcode: '.DUITKU_MERCHANT_CODE
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_POSTFIELDS => json_encode($body)
        ));

        $response = curl_exec($ch);
        $is_success = 1;
        if (curl_errno($ch)) {
            $response = curl_error($ch);
            $is_success = 0;
        }else{
            $response = json_decode($response, TRUE);
        }
        curl_close($ch);

        return array(
            "is_success" => $is_success,
            "response" => $response
        );
    }

    function create_order($invoice_number, $order_amount, $customer_email){
        $time = time().'000';
        $SIGNATURE = hash('sha256', DUITKU_MERCHANT_CODE.$time.DUITKU_MERCHANT_KEY);
        $url = DUITKU_API . '/merchant/createInvoice';
        $body = array(
            'paymentAmount' => intval($invoice_number),
            'merchantOrderId' => $deposit_number,
            'productDetails' => "Order ".$invoice_number,
            'email' => $customer_email,
            'callbackUrl' => DUITKU_ORDER_CALLBACK_URL,
            'returnUrl' => DUITKU_RETURN_URL
        );
        $header = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'x-duitku-signature: '.$SIGNATURE,
            'x-duitku-timestamp: '.$time,
            'x-duitku-merchantcode: '.DUITKU_MERCHANT_CODE
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_POSTFIELDS => json_encode($body)
        ));

        $response = curl_exec($ch);
        $is_success = 1;
        if (curl_errno($ch)) {
            $response = curl_error($ch);
            $is_success = 0;
        }else{
            $response = json_decode($response, TRUE);
        }
        curl_close($ch);

        return array(
            "is_success" => $is_success,
            "response" => $response
        );
    }

?>