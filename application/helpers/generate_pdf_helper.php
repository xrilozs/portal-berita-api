<?php
  require_once APPPATH . '../vendor/autoload.php';

  function generate_invoice($order, $buyer, $config, $is_blob=true){
    $CI =& get_instance();
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);

    $data = array(
      "order" => $order,
      "buyer" => $buyer,
      "config" => $config,
    );

    $html = $CI->load->view('template/invoice', $data, true);

    $mpdf->WriteHTML($html);
    if($is_blob){
      $response = $mpdf->Output('', 'S');
    }else{
      $response = 'assets/document/'.$order->invoice_number.'.pdf';
      $mpdf->Output($response, 'F');
    }

    // $subject = "PDF";
    // $message = $html;
    // $filename = $order->invoice_number . '.pdf';
    // $response = send_email($email, $subject, $message, $attachment, $filename);
    // logging('debug', 'GENERATE PDF', $response);
    
    return $response;
  }
?>