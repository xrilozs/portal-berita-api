<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translateUri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'common';
$route['404_override'] = '';
$route['translateUri_dashes'] = FALSE;
$route['test'] = 'common/test';
$route['test-snap/(:any)'] = 'common/snap/$1';
$route['test-email'] = 'common/test_email';
$route['test-print'] = 'common/test_print';
$route['test-wa'] = 'common/test_wa';
$route['test-file'] = 'common/test_file';


/* API */
$route['send-email']                    = 'common/send_email';

#admin
$route['admin/login']['POST']           = 'admin/login';
$route['admin/refresh']['GET']          = 'admin/refresh';
$route['admin/change-password']['PUT']  = 'admin/change_password';
$route['admin']['GET']                  = 'admin/get_admin';
$route['admin/by-id/(:any)']['GET']     = 'admin/get_admin_by_id/$1';
$route['admin/profile']['GET']          = 'admin/get_profile';
$route['admin']['POST']                 = 'admin/create_admin';
$route['admin']['PUT']                  = 'admin/update_admin';
$route['admin/active/(:any)']['PUT']    = 'admin/active/$1';
$route['admin/inactive/(:any)']['PUT']  = 'admin/inactive/$1';

#Apps
$route['apps']['GET']               = 'apps/get_apps';
$route['apps/all']['GET']           = 'apps/get_apps_all';
$route['apps/by-id/(:any)']['GET']  = 'apps/get_apps_by_id/$1';
$route['apps']['POST']              = 'apps/create_apps';
$route['apps']['PUT']               = 'apps/update_apps';
$route['apps/(:any)']['DELETE']     = 'apps/delete_apps/$1';
$route['apps/upload-icon']['POST']  = 'apps/upload_icon';

#Category News
$route['category_news']['GET']                  = 'category_news/get_category_news';
$route['category_news/all']['GET']              = 'category_news/get_category_news_all';
$route['category_news/by-id/(:any)']['GET']     = 'category_news/get_category_news_by_id/$1';
$route['category_news']['POST']                 = 'category_news/create_category_news';
$route['category_news']['PUT']                  = 'category_news/update_category_news';
$route['category_news/(:any)']['DELETE']        = 'category_news/delete_category_news/$1';

#config
$route['config']['GET']               = 'config/get_config';
$route['config/contact']['PUT']       = 'config/update_contact';
$route['config/social']['PUT']        = 'config/update_social';
$route['config/web']['PUT']           = 'config/update_web';
$route['config/upload-image']['POST'] = 'config/upload_image';

#Document
$route['document']['GET']                   = 'document/get_document';
$route['document/by-id/(:any)']['GET']      = 'document/get_document_by_id/$1';
$route['document/by-alias/(:any)']['GET']   = 'document/get_document_by_alias/$1';
$route['document/count']['GET']             = 'document/count_document';
$route['document']['POST']                  = 'document/create_document';
$route['document']['PUT']                   = 'document/update_document';
$route['document/publish/(:any)']['PUT']    = 'document/publish_document/$1';
$route['document/unpublish/(:any)']['PUT']  = 'document/unpublish_document/$1';
$route['document/(:any)']['DELETE']         = 'document/delete_document/$1';
$route['document/upload-document']['POST']  = 'document/upload_document';

#Gallery Photo
$route['photo']['GET']                  = 'photo/get_photo';
$route['photo/count']['GET']            = 'photo/count_photo';
$route['photo/by-id/(:any)']['GET']     = 'photo/get_photo_by_id/$1';
$route['photo/by-alias/(:any)']['GET']  = 'photo/get_photo_by_alias/$1';
$route['photo']['POST']                 = 'photo/create_photo';
$route['photo']['PUT']                  = 'photo/update_photo';
$route['photo/publish/(:any)']['PUT']   = 'photo/publish_photo/$1';
$route['photo/unpublish/(:any)']['PUT'] = 'photo/unpublish_photo/$1';
$route['photo/(:any)']['DELETE']        = 'photo/delete_photo/$1';
$route['photo/item']['POST']            = 'photo/add_photo_item';
$route['photo/item/(:any)']['DELETE']   = 'photo/remove_photo_item/$1';
$route['photo/upload-image']['POST']    = 'photo/upload_image';

#Gallery Video
$route['video']['GET']                  = 'video/get_video';
$route['video/count']['GET']            = 'video/count_video';
$route['video/by-id/(:any)']['GET']     = 'video/get_video_by_id/$1';
$route['video/by-alias/(:any)']['GET']  = 'video/get_video_by_alias/$1';
$route['video']['POST']                 = 'video/create_video';
$route['video']['PUT']                  = 'video/update_video';
$route['video/publish/(:any)']['PUT']   = 'video/publish_video/$1';
$route['video/unpublish/(:any)']['PUT'] = 'video/unpublish_video/$1';
$route['video/(:any)']['DELETE']        = 'video/delete_video/$1';

#News
$route['news']['GET']                   = 'news/get_news';
$route['news/count']['GET']             = 'news/count_news';
$route['news/by-id/(:any)']['GET']      = 'news/get_news_by_id/$1';
$route['news/by-alias/(:any)']['GET']   = 'news/get_news_by_alias/$1';
$route['news/by-category/(:any)']['GET']= 'news/get_news_by_category/$1';
$route['news']['POST']                  = 'news/create_news';
$route['news']['PUT']                   = 'news/update_news';
$route['news/publish/(:any)']['PUT']    = 'news/publish_news/$1';
$route['news/unpublish/(:any)']['PUT']  = 'news/unpublish_news/$1';
$route['news/(:any)']['DELETE']         = 'news/delete_news/$1';
$route['news/upload-image']['POST']     = 'news/upload_image';

#Profile Page
$route['profile']['GET']                  = 'profile/get_profile';
$route['profile/all']['GET']              = 'profile/get_profile_all';
$route['profile/by-id/(:any)']['GET']     = 'profile/get_profile_by_id/$1';
$route['profile/by-alias/(:any)']['GET']  = 'profile/get_profile_by_alias/$1';
$route['profile']['POST']                 = 'profile/create_profile';
$route['profile']['PUT']                  = 'profile/update_profile';
$route['profile/(:any)']['DELETE']        = 'profile/delete_profile/$1';

#Live Streaming
$route['live_streaming']['GET']                   = 'live_streaming/get_live_streaming';
$route['live_streaming/all']['GET']               = 'live_streaming/get_live_streaming_all';
$route['live_streaming/by-id/(:any)']['GET']      = 'live_streaming/get_live_streaming_by_id/$1';
$route['live_streaming']['POST']                  = 'live_streaming/create_live_streaming';
$route['live_streaming']['PUT']                   = 'live_streaming/update_live_streaming';
$route['live_streaming/publish/(:any)']['PUT']    = 'live_streaming/publish_live_streaming/$1';
$route['live_streaming/unpublish/(:any)']['PUT']  = 'live_streaming/unpublish_live_streaming/$1';
$route['live_streaming/(:any)']['DELETE']         = 'live_streaming/delete_live_streaming/$1';

#Competition Schedule
$route['competition_schedule']['GET']                   = 'competition_schedule/get_competition_schedule';
$route['competition_schedule/active']['GET']            = 'competition_schedule/get_active_competition_schedule';
$route['competition_schedule/by-id/(:any)']['GET']      = 'competition_schedule/get_competition_schedule_by_id/$1';
$route['competition_schedule']['POST']                  = 'competition_schedule/create_competition_schedule';
$route['competition_schedule']['PUT']                   = 'competition_schedule/update_competition_schedule';
$route['competition_schedule/(:any)']['DELETE']         = 'competition_schedule/delete_competition_schedule/$1';
$route['competition_schedule/upload-document']['POST']  = 'competition_schedule/upload_document';
$route['competition_schedule/items/(:any)']['GET']      = 'competition_schedule/get_competition_schedule_item_by_competition_schedule_id/$1';
$route['competition_schedule/batch']['POST']            = 'competition_schedule/batch_competition_schedule_item';
$route['competition_schedule/item/(:any)/(:any)']['GET']= 'competition_schedule/get_competition_schedule_item/$1/$2';
$route['competition_schedule/item']['POST']             = 'competition_schedule/add_competition_schedule_item';
$route['competition_schedule/item']['PUT']              = 'competition_schedule/update_competition_schedule_item';
$route['competition_schedule/item/(:any)']['DELETE']    = 'competition_schedule/remove_competition_schedule_item/$1';
$route['competition_schedule/event']['GET']             = 'competition_schedule/get_competition_event';

#Competition Score
$route['competition_score/cabang']['GET']                     = 'competition_score/get_cabang';
$route['competition_score/golongan-by-cabang/(:any)']['GET']  = 'competition_score/get_golongan_by_cabang/$1';
