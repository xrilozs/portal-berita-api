<?php
  class Kaltim_mtq_event_model extends CI_Model{
    
    function get_mtq_event_all(){
      $query = $this->db->get('kaltim_mtq_event');
      return $query->result();
    }

    function get_active_mtq_event(){
      $this->db->where("event_status", 1);
      $query = $this->db->get('kaltim_mtq_event');
      return $query->num_rows() > 0 ? $query->row() : null;
    }
  }
?>
