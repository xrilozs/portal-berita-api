<?php
  class Kaltim_mtq_golongan_model extends CI_Model{
    
    function get_mtq_golongan_all(){
      $query = $this->db->get('kaltim_mtq_golongan');
      return $query->result();
    }

    function get_mtq_golongan_by_cabang($cabang){
      $this->db->where("cabang_id", $cabang);
      $query = $this->db->get('kaltim_mtq_golongan');
      return $query->result();
    }
  }
?>
