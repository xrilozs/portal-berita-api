<?php
  class Document_model extends CI_Model{
    public $id;
    public $name;
    public $alias;
    public $document_url;
    public $size;
    public $status;

    function get_document($search=null, $status=null, $order=null, $limit=null){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', document_url) as full_document_url
      ");
      if($search){
        $where_search = "(
          (CONCAT_WS(',', name, alias) LIKE '%".$search."%')
        )";
        $this->db->where($where_search);
      }
      if($status){
        $this->db->where("status", $status);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get("lptq_document");
      return $query->result();
    }

    function count_document($search=null, $status=null){
      if($search){
        $where_search = "(
          (CONCAT_WS(',', name, alias) LIKE '%".$search."%')
        )";
        $this->db->where($where_search);
      }
      if($status){
        $this->db->where("status", $status);
      }
      $this->db->from("lptq_document");
      return $this->db->count_all_results();
    }

    function get_document_by_id($id){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', document_url) as full_document_url
      ");
      $this->db->where("id", $id);
      $query = $this->db->get("lptq_document");
      return $query->num_rows() > 0 ? $query->row() : null;
    }
    
    function get_document_by_alias($alias){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', document_url) as full_document_url
      ");
      $this->db->where("alias", $alias);
      $query = $this->db->get("lptq_document");
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_document($data){
      $this->id               = $data['id'];
      $this->name             = $data['name'];
      $this->alias            = $data['alias'];
      $this->document_url     = $data['document_url'];
      $this->size             = $data['size'];
      $this->status           = $data['status'];
      $this->created_at       = date('Y-m-d H:i:s');

      $this->db->insert("lptq_document", $this);
      return $this->db->affected_rows();
    }

    function update_document($data){
      $this->id               = $data['id'];
      $this->name             = $data['name'];
      $this->alias            = $data['alias'];
      $this->document_url     = $data['document_url'];
      $this->size             = $data['size'];
      $this->status           = $data['status'];

      $this->db->update("lptq_document", $this, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

    function unpublish_document($id){
      $data = array(
        "status" => "DRAFT"
      );
      $this->db->where('id', $id);
      $this->db->update("lptq_document", $data);
      return $this->db->affected_rows();
    }

    function publish_document($id){
      $data = array(
        "status" => "PUBLISH"
      );
      $this->db->where('id', $id);
      $this->db->update("lptq_document", $data);
      return $this->db->affected_rows();
    }

    function delete_document($id){
      $this->db->where('id', $id);
      $this->db->delete("lptq_document");
      return $this->db->affected_rows();
    }
  }
?>
