<?php
  class Traffic_gallery_video_model extends CI_Model{
    public $id;
    public $user_id;
    public $message;
    public $is_read;
    public $redirect_link;

    function create_notification($data){
      $this->id = $data['id'];
      $this->user_id = $data['user_id'];
      $this->message = $data['message'];
      $this->is_read = 0;
      $this->redirect_link = array_key_exists('redirect_link', $data) ? $data['redirect_link'] : null;
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('notification', $this);
      return $this->db->affected_rows();
    }

    function read_notification($id){
      $data = array(
        "is_read" => 1
      );
      $this->db->where('id', $id);
      $this->db->update('notification', $data);
      return $this->db->affected_rows();
    }

    function read_all_notification($user_id){
      $data = array(
        "is_read" => 1
      );
      $this->db->where('user_id', $user_id);
      $this->db->update('notification', $data);
      return $this->db->affected_rows();
    }

    function get_notification_by_user_id($is_read=null, $user_id, $order=null, $limit=null){
      $this->db->where("user_id", $user_id);
      if(!is_null($is_read)){
        $this->db->where("is_read", $is_read);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('notification');
      return $query->result();
    }

    function get_notification_by_user_id_and_id($user_id, $id){
      $this->db->where("user_id", $user_id);
      $this->db->where("id", $id);
      $query = $this->db->get('notification');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function count_notification($is_read=0, $user_id){
      $this->db->where("user_id", $user_id);
      $this->db->where("is_read", $is_read);
      $this->db->from('notification');
      return $this->db->count_all_results();
    }
  }
?>
