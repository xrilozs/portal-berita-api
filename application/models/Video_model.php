<?php
  class Video_model extends CI_Model{
    public $id;
    public $title;
    public $alias;
    public $video_url;
    public $description;
    public $status;

    function get_video($search=null, $status=null, $order=null, $limit=null){
      $this->db->select("*");
      if($search){
        $where_search = "(
          (CONCAT_WS(',', title, alias, video_url) LIKE '%".$search."%')
        )";
        $this->db->where($where_search);
      }
      if($status){
        $this->db->where("status", $status);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get("lptq_gallery_video");
      return $query->result();
    }

    function count_video($search=null, $status=null){
      if($search){
        $where_search = "(
          (CONCAT_WS(',', title, alias, video_url) LIKE '%".$search."%')
        )";
        $this->db->where($where_search);
      }
      if($status){
        $this->db->where("status", $status);
      }
      $this->db->from("lptq_gallery_video");
      return $this->db->count_all_results();
    }

    function get_video_by_id($id){
      $this->db->select("*");
      $this->db->where("id", $id);
      $query = $this->db->get("lptq_gallery_video");
      return $query->num_rows() > 0 ? $query->row() : null;
    }
    
    function get_video_by_alias($alias){
      $this->db->select("*");
      $this->db->where("alias", $alias);
      $query = $this->db->get("lptq_gallery_video");
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_video($data){
      $this->id               = $data['id'];
      $this->title            = $data['title'];
      $this->alias            = $data['alias'];
      $this->video_url        = $data['video_url'];
      $this->description      = $data['description'];
      $this->status           = $data['status'];
      $this->created_at       = date('Y-m-d H:i:s');

      $this->db->insert("lptq_gallery_video", $this);
      return $this->db->affected_rows();
    }

    function update_video($data){
      $this->id               = $data['id'];
      $this->title            = $data['title'];
      $this->alias            = $data['alias'];
      $this->video_url        = $data['video_url'];
      $this->description      = $data['description'];
      $this->status           = $data['status'];

      $this->db->update("lptq_gallery_video", $this, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

    function unpublish_video($id){
      $data = array(
        "status" => "DRAFT"
      );
      $this->db->where('id', $id);
      $this->db->update("lptq_gallery_video", $data);
      return $this->db->affected_rows();
    }

    function publish_video($id){
      $data = array(
        "status" => "PUBLISH"
      );
      $this->db->where('id', $id);
      $this->db->update("lptq_gallery_video", $data);
      return $this->db->affected_rows();
    }

    function delete_video($id){
      $this->db->where('id', $id);
      $this->db->delete("lptq_gallery_video");
      return $this->db->affected_rows();
    }
  }
?>
