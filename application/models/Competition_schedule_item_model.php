<?php
  class Competition_schedule_item_model extends CI_Model{
    public $id;
    public $competition_schedule_id;
    public $date;
    public $schedule_json;

    function get_competition_schedule_item_by_id($id){
      $this->db->select("*");
      $this->db->where("id", $id);
      $query = $this->db->get("lptq_competition_schedule_item");
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_competition_schedule_item_by_id_and_schedule_id($id, $schedule_id){
      $this->db->select("*");
      $this->db->where("id", $id);
      $this->db->where("competition_schedule_id", $schedule_id);
      $query = $this->db->get("lptq_competition_schedule_item");
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_competition_schedule_item_by_competition_schedule_id($competition_schedule_id){
      $this->db->select("csi.*, e.event_nama, cs.description");
      $this->db->where("csi.competition_schedule_id", $competition_schedule_id);
      $this->db->join("lptq_competition_schedule cs", "cs.id = csi.competition_schedule_id");
      $this->db->join("kaltim_mtq_event e", "e.event_id = cs.event_id");
      $query = $this->db->get('lptq_competition_schedule_item csi');
      return $query->result();
    }

    function bulk_competition_schedule_item($bulk){
      $this->db->insert_batch("lptq_competition_schedule_item", $bulk); 
      return $this->db->affected_rows();
    }

    function create_competition_schedule_item($data){
      $this->id                       = $data['id'];
      $this->competition_schedule_id  = $data['competition_schedule_id'];
      $this->date                     = $data['date'];
      $this->schedule_json            = $data['schedule_json'];
      $this->created_at               = date('Y-m-d H:i:s');

      $this->db->insert("lptq_competition_schedule_item", $this);
      return $this->db->affected_rows();
    }

    function update_competition_schedule_item($data){
      $this->id                       = $data['id'];
      $this->competition_schedule_id  = $data['competition_schedule_id'];
      $this->date                     = $data['date'];
      $this->schedule_json            = $data['schedule_json'];

      $this->db->update("lptq_competition_schedule_item", $this, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_competition_schedule_item($id){
      $this->db->where('id', $id);
      $this->db->delete("lptq_competition_schedule_item");
      return $this->db->affected_rows();
    }

    function delete_competition_schedule_item_by_competition_schedule_id($competition_schedule_id){
      $this->db->where('competition_schedule_id', $competition_schedule_id);
      $this->db->delete("lptq_competition_schedule_item");
      return $this->db->affected_rows();
    }
  }
?>
