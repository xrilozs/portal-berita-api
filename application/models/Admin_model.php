<?php
  class Admin_model extends CI_Model{
    public $id;
    public $username;
    public $password;
    public $fullname;
    public $role;
    public $is_active;
    
    function get_admin_by_username($username, $is_active=null){
      $this->db->where("username", $username);
      if(!is_null($is_active)){
        $this->db->where("is_active", $is_active);
      }
      $query = $this->db->get("lptq_admin");
      return $query->num_rows() ? $query->row() : null;
    }

    function get_admins($search=null, $is_active=null, $role=null, $order=null, $limit=null){
      if(!is_null($is_active)){
        $this->db->where('is_active', $is_active);
      }
      if($search){
        $where_search = "(
          fullname LIKE '%$search%' OR 
          username LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($role){
        $this->db->where('role', $role);
      }
      $this->db->from("lptq_admin");
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }


    function count_admin($search=null, $is_active=null, $role=null){
      $this->db->from("lptq_admin");
      if($search){
        $where_search = "(
          fullname LIKE '%$search%' OR 
          username LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if(!is_null($is_active)){
        $this->db->where('is_active', $is_active);
      }
      if($role){
        $this->db->where("role", $role);
      }
      return $this->db->count_all_results();
    }

    function get_admin_by_id($id){
      $this->db->where("id", $id);
      $this->db->from("lptq_admin");
      $query = $this->db->get();
      return $query->num_rows() ? $query->row() : null;
    }

    function create_admin($data){
      $this->id = $data['id'];
      $this->password = $data['password'];
      $this->fullname = $data['fullname'];
      $this->username = $data['username'];
      $this->role = $data['role'];
      $this->is_active = 1;
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert("lptq_admin", $this);
      return $this->db->affected_rows() > 0;
    }

    function update_admin($data){
      $this->id = $data['id'];
      $this->password = $data['password'];
      $this->fullname = $data['fullname'];
      $this->username = $data['username'];
      $this->role = $data['role'];
      $this->is_active = $data['is_active'];

      $this->db->update("lptq_admin", $this, array("id"=>$data['id']));
      return $this->db->affected_rows();
    }

    function delete($id){
      $this->db->where('id', $id);
      $this->db->delete("lptq_admin");
      return $this->db->affected_rows();
    }

    function inactive($id){
      $data = array(
        'is_active' => 0,
      );
      $this->db->update("lptq_admin", $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function active($id){
      $data = array(
        'is_active' => 1
      );
      $this->db->update("lptq_admin", $data, array('id' => $id));
      return $this->db->affected_rows();
    }
    
    function change_password($id, $password){
      $req = array(
        'password' => $password
      );
      $this->db->update("lptq_admin", $req, array('id'=>$id));
      return $this->db->affected_rows();
    }
  }
?>
