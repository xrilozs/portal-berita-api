<?php
  class Kaltim_mtq_cabang_model extends CI_Model{
    
    function get_mtq_cabang_all(){
      $query = $this->db->get('kaltim_mtq_cabang');
      return $query->result();
    }

    function get_active_mtq_cabang(){
      $this->db->where("status", 1);
      $query = $this->db->get('kaltim_mtq_cabang');
      return $query->result();
    }
  }
?>
