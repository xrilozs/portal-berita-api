<?php
  class Photo_model extends CI_Model{
    public $id;
    public $title;
    public $alias;
    public $description;
    public $event_date;
    public $thumbnail_url;
    public $status;

    function get_photo($search=null, $status=null, $order=null, $limit=null){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', thumbnail_url) as full_thumbnail_url
      ");
      if($search){
        $where_search = "(
          (CONCAT_WS(',', title, alias, description) LIKE '%".$search."%')
        )";
        $this->db->where($where_search);
      }
      if($status){
        $this->db->where("status", $status);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get("lptq_gallery_photo");
      return $query->result();
    }

    function count_photo($search=null, $status=null){
      if($search){
        $where_search = "(
          (CONCAT_WS(',', title, alias, description) LIKE '%".$search."%')
        )";
        $this->db->where($where_search);
      }
      if($status){
        $this->db->where("status", $status);
      }
      $this->db->from("lptq_gallery_photo");
      return $this->db->count_all_results();
    }

    function get_photo_by_id($id){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', thumbnail_url) as full_thumbnail_url
      ");
      $this->db->where("id", $id);
      $query = $this->db->get("lptq_gallery_photo");
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_photo_by_alias($alias){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', thumbnail_url) as full_thumbnail_url
      ");
      $this->db->where("alias", $alias);
      $query = $this->db->get("lptq_gallery_photo");
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_photo($data){
      $this->id = $data['id'];
      $this->title = $data['title'];
      $this->alias = $data['alias'];
      $this->description = $data['description'];
      $this->event_date = $data['event_date'];
      $this->thumbnail_url = $data['thumbnail_url'];
      $this->status = $data['status'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert("lptq_gallery_photo", $this);
      return $this->db->affected_rows();
    }

    function update_photo($data){
      $this->id = $data['id'];
      $this->title = $data['title'];
      $this->alias = $data['alias'];
      $this->description = $data['description'];
      $this->event_date = $data['event_date'];
      $this->thumbnail_url = $data['thumbnail_url'];
      $this->status = $data['status'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->where('id', $data['id']);
      $this->db->update("lptq_gallery_photo", $this);
      return $this->db->affected_rows();
    }

    function unpublish_photo($id){
      $data = array(
        "status" => "DRAFT"
      );
      $this->db->where('id', $id);
      $this->db->update("lptq_gallery_photo", $data);
      return $this->db->affected_rows();
    }

    function publish_photo($id){
      $data = array(
        "status" => "PUBLISH"
      );
      $this->db->where('id', $id);
      $this->db->update("lptq_gallery_photo", $data);
      return $this->db->affected_rows();
    }

    function delete_photo($id){
      $this->db->where('id', $id);
      $this->db->delete("lptq_gallery_photo");
      return $this->db->affected_rows();
    }
  }
?>
