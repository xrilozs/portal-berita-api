<?php
  class Config_model extends CI_Model{
    public $id;
    public $sm_twitter;
    public $sm_instagram;
    public $sm_facebook;
    public $sm_whatsapp;
    public $sm_youtube;
    public $address;
    public $phone;
    public $email;
    public $google_map;
    public $logo_1_url;
    public $logo_2_url;
    public $icon_url;
    public $title_1;
    public $title_2;

    function get_config(){
      $this->db->from("lptq_config");
      $query = $this->db->get();
      return $query->num_rows() ? $query->row() : null;
    }

    function get_config_by_id($id){
      $this->db->where("id", $id);
      $this->db->from("lptq_config");
      $query = $this->db->get();
      return $query->num_rows() ? $query->row() : null;
    }

    function create_update_config($data){
      $id = $data['id'];
      $this->db->replace("lptq_config", $data, array("id"=>$id));
      return $this->db->affected_rows();
    }
    
  }
?>
