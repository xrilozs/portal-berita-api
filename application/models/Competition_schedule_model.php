<?php
  class Competition_schedule_model extends CI_Model{
    public $id;
    public $event_id;
    public $description;
    public $pdf_url;

    function get_competition_schedule($search=null, $order=null, $limit=null){
      $this->db->select("cs.*, e.event_nama, e.event_status, e.event_tgl_awal, e.event_tgl_akhir, CONCAT('" . BASE_URL . "', pdf_url) as full_pdf_url");
      if($search){
        $where_search = "(
          (CONCAT_WS(',', e.event_nama, cs.description, e.event_tgl_awal, e.event_tgl_akhir) LIKE '%".$search."%')
        )";
        $this->db->where($where_search);
      }
      if($order){
        $this->db->order_by("cs.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $this->db->join("kaltim_mtq_event e", "e.event_id = cs.event_id");
      $query = $this->db->get('lptq_competition_schedule as cs');
      return $query->result();
    }

    function count_competition_schedule($search=null){
      if($search){
        $where_search = "(
          (CONCAT_WS(',', e.event_nama, cs.description, e.event_tgl_awal, e.event_tgl_akhir) LIKE '%".$search."%')
        )";
        $this->db->where($where_search);
      }
      $this->db->from('lptq_competition_schedule cs');
      $this->db->join("kaltim_mtq_event e", "e.event_id = cs.event_id");
      return $this->db->count_all_results();
    }

    function get_competition_schedule_by_id($id){
      $this->db->select("cs.*, e.event_nama, e.event_status, e.event_tgl_awal, e.event_tgl_akhir, CONCAT('" . BASE_URL . "', pdf_url) as full_pdf_url");
      $this->db->where("cs.id", $id);
      $this->db->join("kaltim_mtq_event e", "e.event_id = cs.event_id");
      $query = $this->db->get('lptq_competition_schedule cs');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_competition_schedule_by_event_id($event_id){
      $this->db->select("cs.*, e.event_nama, e.event_status, e.event_tgl_awal, e.event_tgl_akhir, CONCAT('" . BASE_URL . "', pdf_url) as full_pdf_url");
      $this->db->where("cs.event_id", $event_id);
      $this->db->join("kaltim_mtq_event e", "e.event_id = cs.event_id");
      $query = $this->db->get('lptq_competition_schedule cs');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_competition_schedule($data){
      $this->id           = $data['id'];
      $this->event_id     = $data['event_id'];
      $this->description  = $data['description'];
      $this->pdf_url  = $data['pdf_url'];
      $this->created_at   = date('Y-m-d H:i:s');

      $this->db->insert("lptq_competition_schedule", $this);
      return $this->db->affected_rows();
    }

    function update_competition_schedule($data){
      $this->id           = $data['id'];
      $this->event_id     = $data['event_id'];
      $this->description  = $data['description'];
      $this->pdf_url      = $data['pdf_url'];

      $this->db->where('id', $data['id']);
      $this->db->update("lptq_competition_schedule", $this);
      return $this->db->affected_rows();
    }

    function delete_competition_schedule($id){
      $this->db->where('id', $id);
      $this->db->delete("lptq_competition_schedule");
      return $this->db->affected_rows();
    }
  }
?>
