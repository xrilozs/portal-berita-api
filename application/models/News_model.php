<?php
  class News_model extends CI_Model{
    public $id;
    public $category_news_id;
    public $title;
    public $alias;
    public $img_url;
    public $content;
    public $meta_description;
    public $meta_keywords;
    public $creator_id;
    public $approval_id;
    public $last_update_by;
    public $status;

    function get_news($search=null, $category_news_id=null, $status=null, $order=null, $limit=null){
      $this->db->select("*,
        (SELECT bc.name FROM lptq_category_news as bc where bc.id = category_news_id) as category_news_name,
        CONCAT('" . BASE_URL . "', img_url) as full_img_url
      ");
      if($search){
        $where_search = "(
          (CONCAT_WS(',', title, alias, content) LIKE '%".$search."%')
        )";
        $this->db->where($where_search);
      }
      if($category_news_id){
        $this->db->where("category_news_id", $category_news_id);
      }
      if($status){
        $this->db->where("status", $status);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get("lptq_news");
      return $query->result();
    }

    function count_news($search=null, $category_news_id=null, $status=null){
      if($search){
        $where_search = "(
          (CONCAT_WS(',', title, alias, content) LIKE '%".$search."%')
        )";
        $this->db->where($where_search);
      }
      if($category_news_id){
        $this->db->where("category_news_id", $category_news_id);
      }
      if($status){
        $this->db->where("status", $status);
      }
      $this->db->from("lptq_news");
      return $this->db->count_all_results();
    }

    function get_news_by_id($id){
      $this->db->select("*,
        (SELECT bc.name FROM lptq_category_news as bc where bc.id = category_news_id) as category_news_name,
        CONCAT('" . BASE_URL . "', img_url) as full_img_url
      ");
      $this->db->where("id", $id);
      $query = $this->db->get("lptq_news");
      return $query->num_rows() > 0 ? $query->row() : null;
    }
    
    function get_news_by_alias($alias){
      $this->db->select("*,
        (SELECT bc.name FROM lptq_category_news as bc where bc.id = category_news_id) as category_news_name,
        (SELECT bc.alias FROM lptq_category_news as bc where bc.id = category_news_id) as category_news_alias,
        CONCAT('" . BASE_URL . "', img_url) as full_img_url
      ");
      $this->db->where("alias", $alias);
      $query = $this->db->get("lptq_news");
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_news($data){
      $this->id               = $data['id'];
      $this->category_news_id = $data['category_news_id'];
      $this->title            = $data['title'];
      $this->alias            = $data['alias'];
      $this->img_url          = $data['img_url'];
      $this->content          = $data['content'];
      $this->status           = $data['status'];
      $this->meta_description = $data['meta_description'];
      $this->meta_keywords    = $data['meta_keywords'];
      $this->creator_id       = $data['creator_id'];
      if(array_key_exists("approval_id", $data)){
        $this->approval_id = $data['approval_id'];
      }
      $this->meta_keywords    = $data['meta_keywords'];
      $this->created_at       = date('Y-m-d H:i:s');

      $this->db->insert("lptq_news", $this);
      return $this->db->affected_rows();
    }

    function update_news($data){
      $this->id               = $data['id'];
      $this->category_news_id = $data['category_news_id'];
      $this->title            = $data['title'];
      $this->alias            = $data['alias'];
      $this->img_url          = $data['img_url'];
      $this->content          = $data['content'];
      $this->status           = $data['status'];
      $this->meta_description = $data['meta_description'];
      $this->meta_keywords    = $data['meta_keywords'];
      $this->creator_id       = $data['creator_id'];
      $this->approval_id      = $data['approval_id'];
      $this->last_update_by   = $data['last_update_by'];

      $this->db->update("lptq_news", $this, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

    function unpublish_news($id){
      $data = array(
        "status"      => "DRAFT",
        "approval_id" => null
      );
      $this->db->where('id', $id);
      $this->db->update("lptq_news", $data);
      return $this->db->affected_rows();
    }

    function publish_news($id, $admin_id){
      $data = array(
        "status"      => "PUBLISH",
        "approval_id" => $admin_id
      );
      $this->db->where('id', $id);
      $this->db->update("lptq_news", $data);
      return $this->db->affected_rows();
    }

    function delete_news($id){
      $this->db->where('id', $id);
      $this->db->delete("lptq_news");
      return $this->db->affected_rows();
    }
  }
?>
