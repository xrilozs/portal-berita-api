<?php
  class Live_streaming_model extends CI_Model{
    public $id;
    public $title;
    public $alias;
    public $video_url;
    public $status;

    function get_live_streaming($search=null, $status=null, $order=null, $limit=null){
      $this->db->select("*");
      if($search){
        $where_search = "(
          (CONCAT_WS(',', title, alias, video_url) LIKE '%".$search."%')
        )";
        $this->db->where($where_search);
      }
      if($status){
        $this->db->where("status", $status);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get("lptq_live_streaming");
      return $query->result();
    }

    function count_live_streaming($search=null, $status=null){
      if($search){
        $where_search = "(
          (CONCAT_WS(',', title, alias, video_url) LIKE '%".$search."%')
        )";
        $this->db->where($where_search);
      }
      if($status){
        $this->db->where("status", $status);
      }
      $this->db->from("lptq_live_streaming");
      return $this->db->count_all_results();
    }

    function get_live_streaming_by_id($id){
      $this->db->select("*");
      $this->db->where("id", $id);
      $query = $this->db->get("lptq_live_streaming");
      return $query->num_rows() > 0 ? $query->row() : null;
    }
    
    function get_live_streaming_by_alias($alias){
      $this->db->select("*");
      $this->db->where("alias", $alias);
      $query = $this->db->get("lptq_live_streaming");
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_live_streaming($data){
      $this->id               = $data['id'];
      $this->title            = $data['title'];
      $this->alias            = $data['alias'];
      $this->video_url        = $data['video_url'];
      $this->status           = $data['status'];
      $this->created_at       = date('Y-m-d H:i:s');

      $this->db->insert("lptq_live_streaming", $this);
      return $this->db->affected_rows();
    }

    function update_live_streaming($data){
      $this->id               = $data['id'];
      $this->title            = $data['title'];
      $this->alias            = $data['alias'];
      $this->video_url        = $data['video_url'];
      $this->status           = $data['status'];

      $this->db->update("lptq_live_streaming", $this, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

    function unpublish_live_streaming($id){
      $data = array(
        "status" => "DRAFT"
      );
      $this->db->where('id', $id);
      $this->db->update("lptq_live_streaming", $data);
      return $this->db->affected_rows();
    }

    function publish_live_streaming($id){
      $data = array(
        "status" => "PUBLISH"
      );
      $this->db->where('id', $id);
      $this->db->update("lptq_live_streaming", $data);
      return $this->db->affected_rows();
    }

    function delete_live_streaming($id){
      $this->db->where('id', $id);
      $this->db->delete("lptq_live_streaming");
      return $this->db->affected_rows();
    }
  }
?>
