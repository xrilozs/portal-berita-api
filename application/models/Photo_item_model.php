<?php
  class Photo_item_model extends CI_Model{
    public $id;
    public $gallery_photo_id;
    public $photo_url;

    function get_photo_item_by_id($id){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', photo_url) as full_photo_url
      ");
      $this->db->where("id", $id);
      $query = $this->db->get("lptq_gallery_photo_item");
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_photo_item_by_photo_id($gallery_photo_id){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', photo_url) as full_photo_url
      ");
      $this->db->where("gallery_photo_id", $gallery_photo_id);
      $query = $this->db->get("lptq_gallery_photo_item");
      return $query->result();
    }

    function create_photo_item($data){
      $this->id = $data['id'];
      $this->gallery_photo_id = $data['gallery_photo_id'];
      $this->photo_url = $data['photo_url'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert("lptq_gallery_photo_item", $this);
      return $this->db->affected_rows();
    }

    function update_photo_item($data){
      $this->id = $data['id'];
      $this->gallery_photo_id = $data['gallery_photo_id'];
      $this->photo_url = $data['photo_url'];

      $this->db->update("lptq_gallery_photo_item", $this, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_photo_item($id){
      $this->db->where('id', $id);
      $this->db->delete("lptq_gallery_photo_item");
      return $this->db->affected_rows();
    }

    function delete_photo_item_by_photo_id($gallery_photo_id){
      $this->db->where('gallery_photo_id', $gallery_photo_id);
      $this->db->delete("lptq_gallery_photo_item");
      return $this->db->affected_rows();
    }
  }
?>
