<?php
  class Profile_model extends CI_Model{
    public $id;
    public $title;
    public $content;
    public $alias;

    function get_profile($search=null, $order=null, $limit=null){
      if($search){
        $where_search = "(
          (CONCAT_WS(',', title, alias, content) LIKE '%".$search."%')
        )";
        $this->db->where($where_search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get("lptq_profile");
      return $query->result();
    }

    function count_profile($search=null){
      if($search){
        $where_search = "(
          (CONCAT_WS(',', title, alias, content) LIKE '%".$search."%')
        )";
        $this->db->where($where_search);
      }
      $this->db->from("lptq_profile");
      return $this->db->count_all_results();
    }

    function get_profile_by_id($id){
      $this->db->select("*");
      $this->db->where("id", $id);
      $query = $this->db->get("lptq_profile");
      return $query->num_rows() > 0 ? $query->row() : null;
    }
    
    function get_profile_by_alias($alias){
      $this->db->where("alias", $alias);
      $query = $this->db->get("lptq_profile");
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_profile($data){
      $this->id = $data['id'];
      $this->title = $data['title'];
      $this->content = $data['content'];
      $this->alias = $data['alias'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert("lptq_profile", $this);
      return $this->db->affected_rows();
    }

    function update_profile($data){
      $this->id = $data['id'];
      $this->title = $data['title'];
      $this->content = $data['content'];
      $this->alias = $data['alias'];

      $this->db->update("lptq_profile", $this, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_profile($id){
      $this->db->where('id', $id);
      $this->db->delete("lptq_profile");
      return $this->db->affected_rows();
    }
  }
?>
