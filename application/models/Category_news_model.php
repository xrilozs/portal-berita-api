<?php
    class Category_news_model extends CI_Model{
    public $id;
    public $name;
    public $alias;

    function get_category_news($search=null, $order=null, $limit=null){
      if($search){
        $where_search = "(
          (CONCAT_WS(',', name, alias) LIKE '%".$search."%')
        )";
        $this->db->where($where_search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get("lptq_category_news");
      return $query->result();
    }

    function count_category_news($search=null){
      if($search){
        $where_search = "(
          (CONCAT_WS(',', name, alias) LIKE '%".$search."%')
        )";
        $this->db->where($where_search);
      }
      $this->db->from("lptq_category_news");
      return $this->db->count_all_results();
    }

    function get_category_news_by_id($id){
      $this->db->where("id", $id);
      $query = $this->db->get("lptq_category_news");
      return $query->num_rows() > 0 ? $query->row() : null;
    }
    
    function get_category_news_by_alias($alias){
      $this->db->where("alias", $alias);
      $query = $this->db->get("lptq_category_news");
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_category_news($data){
      $this->id         = $data['id'];
      $this->name       = $data['name'];
      $this->alias      = $data['alias'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert("lptq_category_news", $this);
      return $this->db->affected_rows();
    }

    function update_category_news($data){
      $this->id     = $data['id'];
      $this->name   = $data['name'];
      $this->alias  = $data['alias'];

      $this->db->update("lptq_category_news", $this, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_category_news($id){
      $this->db->where('id', $id);
      $this->db->delete("lptq_category_news");
      return $this->db->affected_rows();
    }
  }
?>
