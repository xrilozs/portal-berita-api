<?php
  class Apps_model extends CI_Model{
    public $id;
    public $name;
    public $url;
    public $icon_url;

    function get_apps($search=null, $order=null, $limit=null){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', icon_url) as full_icon_url
      ");
      if($search){
        $where_search = "(
          (CONCAT_WS(',', name, url) LIKE '%".$search."%')
        )";
        $this->db->where($where_search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get("lptq_apps");
      return $query->result();
    }
    
    function get_apps_all(){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', icon_url) as full_icon_url
      ");
      $query = $this->db->get("lptq_apps");
      return $query->result();
    }

    function get_apps_by_id($id){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', icon_url) as full_icon_url
      ");
      $this->db->where("id", $id);
      $query = $this->db->get("lptq_apps");
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function count_apps($search=null){
      $this->db->select("*");
      $this->db->from("lptq_apps");
      if($search){
        $where_search = "(
          (CONCAT_WS(',', name, url) LIKE '%".$search."%')
        )";
        $this->db->where($where_search);
      }
      return $this->db->count_all_results();
    }

    function create_apps($data){
      $this->id = $data['id'];
      $this->name = $data['name'];
      $this->url = $data['url'];
      $this->icon_url = $data['icon_url'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert("lptq_apps", $this);
      return $this->db->affected_rows();
    }

    function update_apps($data){
      $id = $data['id'];
      $this->id = $data['id'];
      $this->name = $data['name'];
      $this->url = $data['url'];
      $this->icon_url = $data['icon_url'];

      $this->db->update("lptq_apps", $this, array('id'=>$id));
      return $this->db->affected_rows();
    }

    function delete_apps($id){
      $this->db->where('id', $id);
      $this->db->delete("lptq_apps");
      return $this->db->affected_rows();
    }
  }
?>
