<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?=COMPANY_NAME;?></title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <style>
    	body{
    	    font-family: Helvetica, sans-serif;
    	    background-color: #F3F0D7; 
		}
		#header {
            text-align: center;
        }
        #content {
            text-align: center;
            font-size: 18px;
        }
		.img_logo {
			width: 100px;
			display: block;
			margin: 0 auto;
		}
		img.center {
			display: block;
			margin: 0 auto;
		}
        #footer {
            padding-top: 40px;
            text-align: center;
            font-family: "Open Sans", sans-serif;
            font-size: 18px;
        }
        #title {
            font-weight: bold;
            color: #FFBF86;
            font-size: 18px;
        }
        #invoice_number{
            font-weight: bold;
        }
        .button {
			background-color: #FFBF86;
			border: none;
			color: white;
			padding: 15px 32px;
			text-align: center;
			text-decoration: none;
			display: inline-block;
			font-size: 22px;
		}
		.order{
		float: left;
	    	position: relative;
	    	width: 100%;
		}
		.column-1 {
			float: left;
			position: relative;
			width: 20%;
		}
		.column-2 {
			float: left;
			position: relative;
			width: 50%;
		}
		.column-3 {
			float: left;
			position: relative;
			width: 30%;
		}
		.item-description{
			margin-top: 0px; 
			font-size: 14px; 
			text-align: left;
		}
		.item-price{
			color: grey
		}
		.item-total-price{
			margin-top: 0px; 
			font-size: 14px;
			font-weight: bold;
		}
		#address{
			margin-top: 20px;
		}
		#address-title{
			font-weight: bold;
		}
		#address-text{
			padding-left: 20px;
			padding-right: 20px;
			font-size: 14px;
			color: grey;
		}
    </style>
</head>
<body style="background-color:#F3F0D7;">
	<table style="width:100%"><tr>
		<td style="width:15%"></td>
		<td style="width:70%; background-color:white; margin: 20px; padding:20px;">
		<div>
			<img src="<?=ASSETS;?>web/logo.png" class="img_logo">
			<div id="header">
				<h1>TERIMA KASIH</h1>
			</div>
			<div id="content">
					<p>
						Anda telah melakukan pemesanan di <?=COMPANY_NAME;?>.<br>
						Pembayaran telah terverifikasi dan pesanan akan kami proses.<br>
						Berikut rincian pesanan Anda.
				</p>
				
					<p id="invoice_number"> No. Invoice: $invoice_number</p>
					$order_items
					<div id="address">
						<p id="address-title"> Rincian Alamat Pengiriman</p>
						<p id="address-text">
							$address
						</p>
					</div>
			</div>
			<div id="footer">
				&copy; Copyright <span id="title"><?=COMPANY_NAME;?></span>. All Rights Reserved
			</div>
		</div>
		<td style="width:15%"></td>
	</tr></table>
</div>
</body>
</html>