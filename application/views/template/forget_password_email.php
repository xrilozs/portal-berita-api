<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?=COMPANY_NAME;?></title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <style>
    	body{
    	    font-family: Helvetica, sans-serif;
		}
		img.center {
			display: block;
			margin: 0 auto;
		}
        #header {
            text-align: center;
        }
        #content {
            text-align: center;
            font-size: 18px;
        }
        .button {
			background-color: #2491ff;
			border: none;
			color: white;
			padding: 15px 32px;
			text-align: center;
			text-decoration: none;
			display: inline-block;
			font-size: 22px;
			cursor: pointer;
			text-decoration: none;
		}
        #footer {
            padding-top: 40px;
            text-align: center;
            font-family: "Open Sans", sans-serif;
            font-size: 18px;
        }
        #title {
            font-weight: bold;
            color: #2491ff;
            font-size: 18px;
        }
    </style>
</head>
<body style="background-color:#c7eaff; display: flex; justify-content: center;">
	<table style="width:100%">
		<tr>
			<td style="width:15%"></td>
			<td style="width:70%; background-color:white; margin: 20px; padding:20px;">
				<div>
					<img src="https://api.guestpost.com/assets/web/logo.png" class="center" style="width:100px;">
					<div id="header">
						<h1>LUPA KATA SANDI</h1>
					</div>
					<div id="content">
							<p>Anda telah melakukan permintaan lupa kata sandi.<br> Klik link berikut untuk melanjutkan proses.</p>
							<a class="button" href="$url">RESET PASSWORD</a>
							<p>Jika Anda tidak merasa melakukan permintaan lupa kata sandi, abaikan pesan ini.</p>
					</div>

					<div id="footer">
						&copy; Copyright <span id="title"><?=COMPANY_NAME;?></span>. All Rights Reserved
					</div>
				</div>
			</td>
			<td style="width:15%"></td>
		</tr>
	</table>
</body>
</html>