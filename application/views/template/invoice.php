<html>
<head>
	<style>
		body {
			font-family: arial;
		}
		#table-list-item {
			font-family: arial, sans-serif;
			border-collapse: collapse;
			width: 100%;
		}
		.column-list-item{
			border: 1px solid #dddddd;
			text-align: left;
			padding: 5px;
      font-size:14px;
		}
		.mb-20{
			margin-bottom:20px;
		}
		.mb-100{
			margin-bottom:100px;
		}
		.bold{
			font-weight: bold;
		}
		.capitalize{
			text-transform: uppercase;
		}
		.text-right{
			text-align:right;
		}
		.text-center{
			text-align:center !important;
		}
		.w-100{
			width:100%;
		}
		.fs-30{
			font-size:30px;
		}
		.fs-14{
			font-size:14px;
		}
	</style>
</head>
<body>
	<div id="header" class="mb-20">
		<table class="w-100">
			<tr>
				<td width="50%">
					<table class="w-100">
						<tr>
							<td width="20%">
								<?php
									if($config && $config->web_logo){
										echo "<img src=".BASE_URL.$config->web_logo." style='width:80px;'>";
									}
								?>
							</td>
							<td width="80%">
								<div>
									<span class="fs-30 capitalize"><?=COMPANY_NAME;?><span><br>
									<span class="fs-14">
										<?php
											if($config && $config->address){
												echo $config->address;
											}
										?>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</td>
				<td width="50%" class="text-right">
					<span class="bold fs-30">INVOICE</span>
				</td>
			</tr>
		</table>
	</div>
	<div id="info" class="mb-20">
		<table class="w-100">
			<tr>
				<td width="50%">
					<span class="bold">Kepada:</span><br>
					<span><?=$buyer->first_name . " " . $buyer->last_name;?></span><br>
					<span><?=$buyer->email;?></span>
				</td>
				<td width="50%" class="text-right">
					<span class="bold">Tanggal:</span><br>
					<span><?=$order->created_at;?></span><br><br>
					<span class="bold">Nomor Invoice:</span><br>
					<span><?=$order->invoice_number;?></span>
				</td>
			</tr>
		</table>
	</div>
	<div id="table" class="mb-20">
		<table class="w-100" id="table-list-item">
			<tr>
				<th class="column-list-item">No</th>
				<th class="column-list-item">Keterangan</th>
				<th class="column-list-item">Jumlah</th>
			</tr>
			<tr>
				<td class='column-list-item'>1</td>
				<td class='column-list-item'>Guest Post <?=$order->website_url;?></td>
				<td class='column-list-item'>Rp <?=number_format(intval($order->guestpost_price));?></td>
			</tr>
			<?php
				if($order->content_price){
					echo "<tr>
						<td class='column-list-item'>2</td>
						<td class='column-list-item'>Konten ".$order->website_url."</td>
						<td class='column-list-item'>Rp ".number_format(intval($order->content_price))."</td>
					</tr>";
				}
			?>
		</table>
	</div>
	<div id="total" class="text-right mb-100">
		<table class="w-100" id="table-list-item">
			<tr>
				<td width="85%" class="text-right">
					<span class="bold">Sub total:</span>
				</td>
				<td width="15%">
					<span>Rp. <?=number_format(intval($order->total));?></span>
				</td>
			</tr>
			<tr>
				<td width="85%" class="text-right">
					<span class="bold">Diskon:</span>
				</td>
				<td width="15%">
					<span>Rp. <?=number_format(intval($order->promo_amount));?></span>
				</td>
			</tr>
			<tr>
				<td width="85%" class="text-right">
					<span class="bold">Total:</span>
				</td>
				<td width="15%">
					<span>Rp. 
					<?= 
							$order->promo_amount ? 
								number_format(intval($order->total) - intval($order->promo_amount)) : 
								number_format(intval($order->total));
					?>
					</span>
				</td>
			</tr>
		</table>
	</div>
	<div id="footer">
		<p class="bold text-center">Terima kasih atas pembelian Anda.</p>
	</div>
</body>
</html>