<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?=COMPANY_NAME;?></title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <style>
    	body{
    	    font-family: Helvetica, sans-serif;
	    }
        img.center {
            display: block;
            margin: 0 auto;
        }
        #header {
            text-align: center;
        }
        #content {
            text-align: center;
            font-size: 18px;
        }
        .button {
            background-color: #2491ff;
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 24px;
            cursor: pointer;
    		text-decoration: none;
        }
        #footer {
            padding-top: 40px;
            text-align: center;
            font-family: "Open Sans", sans-serif;
            font-size: 14px;
        }
        #title {
            font-weight: bold;
            color: #2491ff;
            font-size: 18px;
        }
    </style>
    </head>
    <body style="background-color:#c7eaff;">
    <table width="100%">
      <tr>
        <td style="width:15%"></td>
        <td style="width:70%; background-color:white; margin: 20px; padding:20px;">
            <div>
                <img src="<?=ASSETS;?>web/logo.svg" class="center" style="width:100px;">
                <div id="header">
                    <h1>SELAMAT</h1>
                </div>
                <div id="content">
                    <p>Anda mendapatkan pesanan di $url.<br>
                    Anda dapat memutuskan untuk menerima atau menolak pesanan dalam waktu 3 hari.</p>
                    <a class="button" href="$web_url">Lihat Pesanan</a>
                </div>
                
                <div id="footer">
                    &copy; Copyright <span id="title"><?=COMPANY_NAME;?></span>. All Rights Reserved
                </div>
            </div>
        </td>
        <td style="width:15%"></td>
      </tr>
    </table>
    </body>
</html>