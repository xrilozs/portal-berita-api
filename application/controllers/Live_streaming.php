<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Live_streaming extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /live_streaming [GET]
    function get_live_streaming(){
        #init req & resp
        $resp_obj           = new Response_api();
        $page_number        = $this->input->get('page_number');
        $page_size          = $this->input->get('page_size');
        $search             = $this->input->get('search');
        $status             = $this->input->get('status');
        $draw               = $this->input->get('draw');
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/live_streaming [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #get live_streaming
        $start              = $page_number * $page_size;
        $order              = array('field'=>'created_at', 'order'=>'DESC');
        $limit              = array('start'=>$start, 'size'=>$page_size);
        $live_streaming     = $this->live_streaming_model->get_live_streaming($search, $status, $order, $limit);
        $total              = $this->live_streaming_model->count_live_streaming($search, $status);

        #response
        if(empty($draw)){
            logging('debug', '/live_streaming [GET] - Get live_streaming is success');
            $resp_obj->set_response(200, "success", "Get live_streaming is success", $live_streaming);
            set_output($resp_obj->get_response());
            return;
        }else{
            logging('debug', '/live_streaming [GET] - Get live_streaming is success');
            $resp_obj->set_response_datatable(200, $live_streaming, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        } 
    }

    #path: /live_streaming/aa [GET]
    function get_live_streaming_all(){
        #init req & resp
        $resp_obj = new Response_api();
        

        #get live_streaming
        $order              = array('field'=>'created_at', 'order'=>'DESC');
        $live_streaming     = $this->live_streaming_model->get_live_streaming(null, "PUBLISH", $order);

        #response
        logging('debug', '/live_streaming/all [GET] - Get live_streaming all is success');
        $resp_obj->set_response(200, "success", "Get live_streaming all is success", $live_streaming);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /live_streaming/by-id/$id [GET]
    function get_live_streaming_by_id($id){
        $resp_obj = new Response_api();

        #get live streaming detail
        $live_streaming = $this->live_streaming_model->get_live_streaming_by_id($id);
        if(is_null($live_streaming)){
            logging('error', '/live_streaming/by-id/'.$id.' [GET] - live streaming not found');
            $resp_obj->set_response(404, "failed", "live streaming not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/live_streaming/by-id/'.$id.' [GET] - Get live streaming by id success');
        $resp_obj->set_response(200, "success", "Get live streaming by id success", $live_streaming);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /live_streaming [POST]
    function create_live_streaming(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/live_streaming [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('title', 'video_url', 'status');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/live_streaming [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id']          = get_uniq_id();
        $request['alias']       = generate_alias($request['title']);
    
        #check live_streaming exist
        $live_streaming_exist = $this->live_streaming_model->get_live_streaming_by_alias($request['alias']);
        if($live_streaming_exist){
            logging('error', '/live_streaming [POST] - Duplicate live streaming title', $request);
            $resp_obj->set_response(400, "failed", "Duplicate live streaming title");
            set_output($resp_obj->get_response());
            return;
        }

        #create live_streaming
        $flag = $this->live_streaming_model->create_live_streaming($request);
        
        #response
        if(!$flag){
            logging('error', '/live_streaming [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/live_streaming [POST] - Create live streaming success', $request);
        $resp_obj->set_response(200, "success", "Create live streaming success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /live_streaming [PUT]
    function update_live_streaming(){
        $resp_obj       = new Response_api();
        $request        = json_decode($this->input->raw_input_stream, true);
        $allowed_role   = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/live_streaming [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'title', 'video_url');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/live_streaming [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check live streaming
        $live_streaming = $this->live_streaming_model->get_live_streaming_by_id($request['id']);
        if(is_null($live_streaming)){
            logging('error', '/live_streaming [PUT] - live streaming not found', $request);
            $resp_obj->set_response(404, "failed", "live streaming not found");
            set_output($resp_obj->get_response());
            return;
        }

        $request['alias'] = generate_alias($request['title']);
        if($request['alias'] != $live_streaming->alias){
            $live_streaming_exist = $this->live_streaming_model->get_live_streaming_by_alias($request['alias']);
            if($live_streaming_exist){
                logging('error', '/live_streaming [PUT] - Duplicate live streaming title', $request);
                $resp_obj->set_response(400, "failed", "Duplicate live streaming title");
                set_output($resp_obj->get_response());
                return;
            }
        }

        #update live_streaming
        $request['status']  = $live_streaming->status;
        $flag               = $this->live_streaming_model->update_live_streaming($request);
        
        #response
        if(empty($flag)){
            logging('error', '/live_streaming [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/live_streaming [PUT] - Update live streaming success', $request);
        $resp_obj->set_response(200, "success", "Update live streaming success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /live_streaming/publish/$id [PUT]
    function publish_live_streaming($id){
        $resp_obj       = new Response_api();
        $allowed_role   = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/live_streaming/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check live streaming
        $live_streaming = $this->live_streaming_model->get_live_streaming_by_id($id);
        if(is_null($live_streaming)){
            logging('error', '/live_streaming/publish/'.$id.' [PUT] - live streaming not found');
            $resp_obj->set_response(404, "failed", "live streaming not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update live streaming
        $flag = $this->live_streaming_model->publish_live_streaming($id);
        
        #response
        if(empty($flag)){
            logging('error', '/live_streaming/publish/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/live_streaming/publish/'.$id.' [PUT] - Publish live streaming success');
        $resp_obj->set_response(200, "success", "Publish live streaming success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /live_streaming/unpublish/$id [PUT]
    function unpublish_live_streaming($id){
        $resp_obj       = new Response_api();
        $allowed_role   = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/live_streaming/unpublish/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check live streaming
        $live_streaming = $this->live_streaming_model->get_live_streaming_by_id($id);
        if(is_null($live_streaming)){
            logging('error', '/live_streaming/unpublish/'.$id.' [PUT] - live streaming not found');
            $resp_obj->set_response(404, "failed", "live streaming not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update live streaming
        $flag = $this->live_streaming_model->unpublish_live_streaming($id);
        
        #response
        if(empty($flag)){
            logging('error', '/live_streaming/unpublish/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/live_streaming/unpublish/'.$id.' [PUT] - Unpublish live streaming success');
        $resp_obj->set_response(200, "success", "Unpublish live streaming success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /live_streaming/$id [DELETE]
    function delete_live_streaming($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/live_streaming/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check live_streaming
        $live_streaming = $this->live_streaming_model->get_live_streaming_by_id($id);
        if(is_null($live_streaming)){
            logging('error', '/live_streaming/'.$id.' [DELETE] - live streaming not found');
            $resp_obj->set_response(404, "failed", "live streaming not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update live_streaming
        $flag = $this->live_streaming_model->delete_live_streaming($id);
        
        #response
        if(empty($flag)){
            logging('error', '/live_streaming/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/live_streaming/'.$id.' [DELETE] - Delete live streaming success');
        $resp_obj->set_response(200, "success", "Delete live streaming success");
        set_output($resp_obj->get_response());
        return;
    }
}
