<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class News extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /news [GET]
    function get_news(){
        #init req & resp
        $resp_obj           = new Response_api();
        $page_number        = $this->input->get('page_number');
        $page_size          = $this->input->get('page_size');
        $search             = $this->input->get('search');
        $status             = $this->input->get('status');
        $category_news_alias= $this->input->get('category_news_alias');
        $draw               = $this->input->get('draw');
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/news [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        $category_news_id = null;
        if($category_news_alias){
            $category_news = $this->category_news_model->get_category_news_by_alias($category_news_alias);
            if(is_null($category_news)){
                logging('debug', '/news [GET] - News category is not found');
                $resp_obj->set_response(404, "failed", "News category is not found");
                set_output($resp_obj->get_response());
                return;
            }
            $category_news_id = $category_news->id;
        }

        #get news
        $start  = $page_number * $page_size;
        $order  = array('field'=>'created_at', 'order'=>'DESC');
        $limit  = array('start'=>$start, 'size'=>$page_size);
        $news   = $this->news_model->get_news($search, $category_news_id, $status, $order, $limit);
        $total  = $this->news_model->count_news($search, $category_news_id, $status);

        #response
        if(empty($draw)){
            logging('debug', '/news [GET] - Get news is success');
            $resp_obj->set_response(200, "success", "Get news is success", $news);
            set_output($resp_obj->get_response());
            return;
        }else{
            logging('debug', '/news [GET] - Get news is success');
            $resp_obj->set_response_datatable(200, $news, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        } 
    }

    #path: /news/count [GET]
    function count_news(){
        #init req & resp
        $resp_obj           = new Response_api();
        $status             = $this->input->get('status');

        #check token
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/news/count [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get news
        $total  = $this->news_model->count_news(null, null, $status);

        #response
        logging('debug', '/news/count [GET] - Count news is success');
        $resp_obj->set_response(200, "success", "Count news is success", $total);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /news/by-id/$id [GET]
    function get_news_by_id($id){
        $resp_obj = new Response_api();

        #get news detail
        $news = $this->news_model->get_news_by_id($id);
        if(is_null($news)){
            logging('error', '/news/by-id/'.$id.' [GET] - news not found');
            $resp_obj->set_response(404, "failed", "news not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/news/by-id/'.$id.' [GET] - Get news by id success');
        $resp_obj->set_response(200, "success", "Get news by id success", $news);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /news/by-alias/$alias [GET]
    function get_news_by_alias($alias){
        $resp_obj = new Response_api();

        #get news detail
        $news = $this->news_model->get_news_by_alias($alias);
        if(is_null($news)){
            logging('error', '/news/by-alias/'.$alias.' [GET] - news not found');
            $resp_obj->set_response(404, "failed", "news not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/news/by-alias/'.$alias.' [GET] - Get news by alias success');
        $resp_obj->set_response(200, "success", "Get news by alias success", $news);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /news [POST]
    function create_news(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/news [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $admin = $resp['data']['admin'];
        
        #check request params
        $keys = array('category_news_id', 'title', 'img_url', 'content', 'status', 'meta_description', 'meta_keywords');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/news [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check news category
        $category_news = $this->category_news_model->get_category_news_by_id($request['category_news_id']);
        if(is_null($category_news)){
            logging('error', '/news [POST] - News category not found', $request);
            $resp_obj->set_response(400, "failed", "News category not found");
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id']          = get_uniq_id();
        $request['alias']       = generate_alias($request['title']);
        $request['creator_id']  = $admin->id;
        if($request['status'] == 'PUBLISH'){
            $request['approval_id'] = $admin->id;
        }
    
        #check news exist
        $news_exist = $this->news_model->get_news_by_alias($request['alias']);
        if($news_exist){
            logging('error', '/news [POST] - Duplicate news title', $request);
            $resp_obj->set_response(400, "failed", "Duplicate news title");
            set_output($resp_obj->get_response());
            return;
        }

        #create news
        $flag = $this->news_model->create_news($request);
        
        #response
        if(!$flag){
            logging('error', '/news [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/news [POST] - Create news success', $request);
        $resp_obj->set_response(200, "success", "Create news success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /news [PUT]
    function update_news(){
        $resp_obj = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/news [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $admin = $resp['data']['admin'];
        
        #check request params
        $keys = array('id', 'category_news_id', 'title', 'img_url', 'content', 'status', 'meta_description', 'meta_keywords');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/news [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check news category
        $category_news = $this->category_news_model->get_category_news_by_id($request['category_news_id']);
        if(is_null($category_news)){
            logging('error', '/news [POST] - News category not found', $request);
            $resp_obj->set_response(400, "failed", "News category not found");
            set_output($resp_obj->get_response());
            return;
        }

        #check news
        $news = $this->news_model->get_news_by_id($request['id']);
        if(is_null($news)){
            logging('error', '/news [PUT] - news not found', $request);
            $resp_obj->set_response(404, "failed", "news not found");
            set_output($resp_obj->get_response());
            return;
        }

        $request['alias'] = generate_alias($request['title']);
        if($request['alias'] != $news->alias){
            $news_exist = $this->news_model->get_news_by_alias($request['alias']);
            if($news_exist){
                logging('error', '/news [PUT] - news name duplicate', $request);
                $resp_obj->set_response(400, "failed", "news name duplicate");
                set_output($resp_obj->get_response());
                return;
            }
        }
        $request['creator_id']      = $news->creator_id;
        $request['approval_id']     = $news->approval_id;
        $request['last_update_by']  = $admin->id;

        #update news
        $flag = $this->news_model->update_news($request);
        
        #response
        if(empty($flag)){
            logging('error', '/news [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/news [PUT] - Update news success', $request);
        $resp_obj->set_response(200, "success", "Update news success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /news/publish/$id [PUT]
    function publish_news($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/news/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $admin = $resp['data']['admin'];

        #check news
        $news = $this->news_model->get_news_by_id($id);
        if(is_null($news)){
            logging('error', '/news/publish/'.$id.' [PUT] - news not found');
            $resp_obj->set_response(404, "failed", "news not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update news
        $flag = $this->news_model->publish_news($id, $admin->id);
        
        #response
        if(empty($flag)){
            logging('error', '/news/publish/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/news/publish/'.$id.' [PUT] - Publish news success');
        $resp_obj->set_response(200, "success", "Publish news success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /news/unpublish/$id [PUT]
    function unpublish_news($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/news/unpublish/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check news
        $news = $this->news_model->get_news_by_id($id);
        if(is_null($news)){
            logging('error', '/news/unpublish/'.$id.' [PUT] - news not found');
            $resp_obj->set_response(404, "failed", "news not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update news
        $flag = $this->news_model->unpublish_news($id);
        
        #response
        if(empty($flag)){
            logging('error', '/news/unpublish/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/news/unpublish/'.$id.' [PUT] - Unpublish news success');
        $resp_obj->set_response(200, "success", "Unpublish news success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /news/$id [DELETE]
    function delete_news($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/news/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check news
        $news = $this->news_model->get_news_by_id($id);
        if(is_null($news)){
            logging('error', '/news/'.$id.' [DELETE] - news not found');
            $resp_obj->set_response(404, "failed", "news not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update news
        $flag = $this->news_model->delete_news($id);
        
        #response
        if(empty($flag)){
            logging('error', '/news/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/news/'.$id.' [DELETE] - Delete news success');
        $resp_obj->set_response(200, "success", "Delete news success");
        set_output($resp_obj->get_response());
        return;
    }
  
    #path: /news/upload-image [POST]
    function upload_image(){
        #init variable
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/news/upload-image [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/img/news/';
        if (empty($_FILES['image']['name'])) {
            logging('error', '/news/upload-image [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload image
        $file = $_FILES['image'];
        $resp = upload_image($file, $destination, false);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/news/upload-image [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }
        $data = $resp['data'];
        $data['full_img_url'] = BASE_URL . $data['img_url'];
        logging('debug', '/news/upload-image [POST] - Upload news image success', $data);
        $resp_obj->set_response(200, "success", "Upload news image success", $data);
        set_output($resp_obj->get_response());
        return; 
    }
}
