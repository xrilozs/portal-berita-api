<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Competition_schedule extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /competition_schedule/active [GET]
    function get_active_competition_schedule(){
        #init req & resp
        $resp_obj       = new Response_api();
        $active_event   = $this->kaltim_mtq_event_model->get_active_mtq_event();
        if(is_null($active_event)){
            logging('error', "/competition_schedule/active [GET] - No active event");
            $resp_obj->set_response(400, "failed", "No active event");
            set_output($resp_obj->get_response());
            return;
        }

        #get competition schedule
        $schedule_event = $this->competition_schedule_model->get_competition_schedule_by_event_id($active_event->event_id);

        #response
        if(is_null($schedule_event)){
            logging('debug', '/competition_schedule/active [GET] - Schedule not found');
            $resp_obj->set_response(404, "success", "Schedule not found");
            set_output($resp_obj->get_response());
            return;
        }

        $schedule_event_items   = $this->competition_schedule_item_model->get_competition_schedule_item_by_competition_schedule_id($schedule_event->id);
        $schedule_event->items  = $schedule_event_items;


        logging('debug', '/competition_schedule/active [GET] - Get active competition schedule is success');
        $resp_obj->set_response(200, "success", "Get active competition schedule is success", $schedule_event);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /competition_schedule [GET]
    function get_competition_schedule(){
        #init req & resp
        $resp_obj           = new Response_api();
        $page_number        = $this->input->get('page_number');
        $page_size          = $this->input->get('page_size');
        $search             = $this->input->get('search');
        $draw               = $this->input->get('draw');

        #check token
        $allowed_role   = array('SUPERADMIN', 'ADMIN', 'USER');
        $header         = $this->input->request_headers();
        $resp           = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/competition_schedule [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/competition_schedule [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #get competition schedule
        $start                  = $page_number * $page_size;
        $order                  = array('field'=>'created_at', 'order'=>'DESC');
        $limit                  = array('start'=>$start, 'size'=>$page_size);
        $competition_schedule   = $this->competition_schedule_model->get_competition_schedule($search, $order, $limit);
        $total                  = $this->competition_schedule_model->count_competition_schedule($search);

        #response
        if(empty($draw)){
            logging('debug', '/competition_schedule [GET] - Get competition schedule is success');
            $resp_obj->set_response(200, "success", "Get competition schedule is success", $competition_schedule);
            set_output($resp_obj->get_response());
            return;
        }else{
            logging('debug', '/competition_schedule [GET] - Get competition schedule is success');
            $resp_obj->set_response_datatable(200, $competition_schedule, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        }
    }

    #path: /competition_schedule/by-id/$id [GET]
    function get_competition_schedule_by_id($id){
        $resp_obj = new Response_api();
        
        #check token
        $allowed_role   = array('SUPERADMIN', 'ADMIN', 'USER');
        $header         = $this->input->request_headers();
        $resp           = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/competition_schedule/by-id/$id [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get competition schedule detail
        $competition_schedule = $this->competition_schedule_model->get_competition_schedule_by_id($id);
        if(is_null($competition_schedule)){
            logging('error', '/competition_schedule/by-id/'.$id.' [GET] - competition schedule not found');
            $resp_obj->set_response(404, "failed", "competition_schedule not found");
            set_output($resp_obj->get_response());
            return;
        }

        $competition_schedule_items     = $this->competition_schedule_item_model->get_competition_schedule_item_by_competition_schedule_id($id);
        $competition_schedule->items    = $competition_schedule_items;

        #response
        logging('debug', '/competition_schedule/by-id/'.$id.' [GET] - Get competition schedule by id success');
        $resp_obj->set_response(200, "success", "Get competition schedule by id success", $competition_schedule);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /competition_schedule [POST]
    function create_competition_schedule(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $allowed_role   = array('SUPERADMIN', 'ADMIN', 'USER');
        $header         = $this->input->request_headers();
        $resp           = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/competition_schedule [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('event_id', 'description', 'pdf_url');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/competition_schedule [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id'] = get_uniq_id();

        #create competition schedule
        $flag = $this->competition_schedule_model->create_competition_schedule($request);
        
        #response
        if(!$flag){
            logging('error', '/competition_schedule [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/competition_schedule [POST] - Create competition schedule success', $request);
        $resp_obj->set_response(200, "success", "Create competition schedule success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /competition_schedule [PUT]
    function update_competition_schedule(){
        $resp_obj       = new Response_api();
        $request        = json_decode($this->input->raw_input_stream, true);
        $allowed_role   = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/competition_schedule [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'event_id', 'description', 'pdf_url');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/competition_schedule [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check competition schedule
        $competition_schedule = $this->competition_schedule_model->get_competition_schedule_by_id($request['id']);
        if(is_null($competition_schedule)){
            logging('error', '/competition_schedule [PUT] - competition schedule not found', $request);
            $resp_obj->set_response(404, "failed", "competition_schedule not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update competition schedule
        $flag = $this->competition_schedule_model->update_competition_schedule($request);
        
        #response
        if(empty($flag)){
            logging('error', '/competition_schedule [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/competition_schedule [PUT] - Update competition schedule success', $request);
        $resp_obj->set_response(200, "success", "Update competition schedule success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /competition_schedule/$id [DELETE]
    function delete_competition_schedule($id){
        $resp_obj       = new Response_api();
        $allowed_role   = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/competition_schedule/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check competition schedule
        $competition_schedule = $this->competition_schedule_model->get_competition_schedule_by_id($id);
        if(is_null($competition_schedule)){
            logging('error', '/competition_schedule/'.$id.' [DELETE] - competition schedule not found');
            $resp_obj->set_response(404, "failed", "competition schedule not found");
            set_output($resp_obj->get_response());
            return;
        }

        #delete competition schedule item
        $flag = $this->competition_schedule_item_model->delete_competition_schedule_item_by_competition_schedule_id($id);
        // if(empty($flag)){
        //     logging('error', '/competition_schedule/'.$id.' [DELETE] - Internal server error ON DELETE SCHEDULE ITEM');
        //     $resp_obj->set_response(500, "failed", "Internal server error");
        //     set_output($resp_obj->get_response());
        //     return;
        // }

        #delete competition schedule
        $flag = $this->competition_schedule_model->delete_competition_schedule($id);
        if(empty($flag)){
            logging('error', '/competition_schedule/'.$id.' [DELETE] - Internal server error ON DELETE SCHEDULE');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/competition_schedule/'.$id.' [DELETE] - Delete competition schedule success');
        $resp_obj->set_response(200, "success", "Delete competition schedule success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /competition_schedule/upload-document [POST]
    function upload_document(){
        #init variable
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/competition_schedule/upload-document [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/document/';
        if (empty($_FILES['doc']['name'])) {
            logging('error', '/document/upload-document [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload document
        $file = $_FILES['doc'];
        $resp = upload_file($file, $destination, 500097152);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/competition_schedule/upload-document [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }

        $data['size']               = convertByte($file['size']);
        $data['document_url']       = $resp['data'];
        $data['full_document_url']  = BASE_URL . $resp['data'];
        logging('debug', '/competition_schedule/upload-document [POST] - Upload document success', $data);
        $resp_obj->set_response(200, "success", "Upload document success", $data);
        set_output($resp_obj->get_response());
        return; 
    }

    #path: /competition_schedule/items/$id [GET]
    function get_competition_schedule_item_by_competition_schedule_id($id){
        $resp_obj = new Response_api();
        
        #check token
        $allowed_role   = array('SUPERADMIN', 'ADMIN', 'USER');
        $header         = $this->input->request_headers();
        $resp           = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/competition_schedule/items/$id [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        $draw = $this->input->get('draw');

        #get competition schedule detail
        // $competition_schedule = $this->competition_schedule_model->get_competition_schedule_by_id($id);
        // if(is_null($competition_schedule)){
        //     logging('error', '/competition_schedule/items/$id [GET] - competition schedule not found');
        //     $resp_obj->set_response(404, "failed", "competition_schedule not found");
        //     set_output($resp_obj->get_response());
        //     return;
        // }

        $competition_schedule_items = $this->competition_schedule_item_model->get_competition_schedule_item_by_competition_schedule_id($id);
        $total = sizeof($competition_schedule_items);

        #response
        if(!$draw){
            logging('debug', '/competition_schedule/items/$id [GET] - Get competition schedule items by competition schedule id success');
            $resp_obj->set_response(200, "success", "Get competition schedule items by competition schedule id success", $competition_schedule_items);
            set_output($resp_obj->get_response());
            return;
        }else{
            logging('debug', '/competition_schedule/items/$id [GET] - Get competition schedule items by competition schedule id is success');
            $resp_obj->set_response_datatable(200, $competition_schedule_items, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        }
    }

    #path: /competition_schedule/batch [POST]
    function batch_competition_schedule_item(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $allowed_role   = array('SUPERADMIN', 'ADMIN', 'USER');
        $header         = $this->input->request_headers();
        $resp           = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/competition_schedule/batch [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('competition_schedule_items');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/competition_schedule/batch [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }
        $competition_schedule_items = $request['competition_schedule_items'];
        foreach ($competition_schedule_items as &$item) {
            $keys = array('competition_schedule_id', 'date', 'schedule_json');
            if(!check_parameter_by_keys($request, $keys)){
                logging('error', '/competition_schedule/batch [POST] - Missing parameter. please check API documentation', $request);
                $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
                set_output($resp_obj->get_response());
                return;
            }
            $item['id'] = get_uniq_id();
        }


        #bulk competition schedule
        $flag = $this->competition_schedule_item_model->bulk_competition_schedule_item($competition_schedule_items);
        
        #response
        if(!$flag){
            logging('error', '/competition_schedule/batch [POST] - Internal server error', $competition_schedule_items);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/competition_schedule/batch [POST] - Batch competition schedule item success', $competition_schedule_items);
        $resp_obj->set_response(200, "success", "Batch competition schedule item success", $competition_schedule_items);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /competition_schedule/item/$1/$2 [GET]
    function get_competition_schedule_item($schedule_id, $item_id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/competition_schedule/item/$1/$2 [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check competition schedule item
        $competition_schedule_item = $this->competition_schedule_item_model->get_competition_schedule_item_by_id_and_schedule_id($item_id, $schedule_id);
        if(is_null($competition_schedule_item)){
            logging('error', '/competition_schedule/item/$1/$2 [GET] - competition schedule item not found');
            $resp_obj->set_response(404, "failed", "competition_schedule item not found");
            set_output($resp_obj->get_response());
            return;
        }

        logging('debug', '/competition_schedule/item/$1/$2 [GET] - Get competition schedule item success');
        $resp_obj->set_response(200, "success", "Get competition schedule item success", $competition_schedule_item);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /competition_schedule/item [POST]
    function add_competition_schedule_item(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $allowed_role   = array('SUPERADMIN', 'ADMIN', 'USER');
        $header         = $this->input->request_headers();
        $resp           = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/competition_schedule/item [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('competition_schedule_id', 'date', 'schedule_json');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/competition_schedule/item [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }


        #create competition schedule
        $request['id']  = get_uniq_id();
        $flag           = $this->competition_schedule_item_model->create_competition_schedule_item($request);
        
        #response
        if(!$flag){
            logging('error', '/competition_schedule/item [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/competition_schedule/item [POST] - Add competition schedule item success', $request);
        $resp_obj->set_response(200, "success", "Add competition schedule item success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /competition_schedule/item [PUT]
    function update_competition_schedule_item(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $allowed_role   = array('SUPERADMIN', 'ADMIN', 'USER');
        $header         = $this->input->request_headers();
        $resp           = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/competition_schedule/item [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'competition_schedule_id', 'date', 'schedule_json');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/competition_schedule/item [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        $comptetition_schedule_item = $this->competition_schedule_item_model->get_competition_schedule_item_by_id($request['id']);
        if(is_null($comptetition_schedule_item)){
            logging('error', '/competition_schedule/item [PUT] - Competition Schedule Item not found', $request);
            $resp_obj->set_response(500, "failed", "Competition Schedule Item not found");
            set_output($resp_obj->get_response());
            return;
        }

        #create competition schedule
        $flag = $this->competition_schedule_item_model->update_competition_schedule_item($request);
        
        #response
        if(!$flag){
            logging('error', '/competition_schedule/item [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/competition_schedule/item [PUT] - Update competition schedule item success', $request);
        $resp_obj->set_response(200, "success", "Update competition schedule item success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /competition_schedule/item/$id [DELETE]
    function remove_competition_schedule_item($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/competition_schedule/item/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check competition schedule item
        $competition_schedule = $this->competition_schedule_item_model->get_competition_schedule_item_by_id($id);
        if(is_null($competition_schedule)){
            logging('error', '/competition_schedule/item/'.$id.' [DELETE] - competition schedule not found');
            $resp_obj->set_response(404, "failed", "competition_schedule not found");
            set_output($resp_obj->get_response());
            return;
        }

        #delete competition schedule item
        $flag = $this->competition_schedule_item_model->delete_competition_schedule_item($id);
        if(empty($flag)){
            logging('error', '/competition_schedule/item/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/competition_schedule/item/'.$id.' [DELETE] - Delete competition schedule item success');
        $resp_obj->set_response(200, "success", "Delete competition schedule item success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /competition_schedule/event [GET]
    function get_competition_event(){
        #init req & resp
        $resp_obj   = new Response_api();
        $events     = $this->kaltim_mtq_event_model->get_mtq_event_all();

        logging('debug', '/competition_schedule/active [GET] - Get competition event is success');
        $resp_obj->set_response(200, "success", "Get competition event is success", $events);
        set_output($resp_obj->get_response());
        return;
    }
}
