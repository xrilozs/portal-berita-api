<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Common extends CI_Controller {
  public function __construct($config = 'rest'){
    parent::__construct($config);
  }
  
  function index(){
    $respObj = new Response_api();
    $respObj->set_response(200, "success", "Not Implemented.");
    $resp = $respObj->get_response();
    set_output($resp);
  }
  
  function test(){
    $respObj = new Response_api();
    $respObj->set_response(200, "success", "APIs is working.");
    $resp = $respObj->get_response();
    set_output($resp);
  }
  
  function test_email(){
    // $template = EMAIL_TEMPLATE;
    $content = "test";
    // $content = str_replace('${content}', $content, $template);
    $mail = new Send_mail();
    $emailResp = $mail->send('xrilozs@gmail.com', "TEST EMAIL", $content);
    
    $respObj = new Response_api();
    $respObj->set_response(200, "success", "Send email...", $emailResp);
    $resp = $respObj->get_response();
    set_output($resp);
  }

  function send_email(){
    $request = json_decode($this->input->raw_input_stream, true);

    #check request params
    $keys = array('name', 'email', 'message');
    if(!check_parameter_by_keys($request, $keys)){
        logging('error', '/send-email [POST] - Missing parameter. please check API documentation', $request);
        $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
        set_output($resp_obj->get_response());
        return;
    }

    $config = $this->config_model->get_config();
    if(is_null($config)){
      $server_mail = $config->email ? $config->email : COMPANY_ADMIN_EMAIL;
    }else{
      $server_mail = COMPANY_ADMIN_EMAIL;
    }

    $content = "Dari ".$request['name']."\r\n".$request['message'];
    // $content = str_replace('${content}', $content, $template);
    $mail = new Send_mail();
    $emailResp = $mail->send($server_mail, "Kontak dari website", $content, null, null, $request['email']);
    
    $respObj = new Response_api();
    $respObj->set_response(200, "success", "Send email...", $emailResp);
    $resp = $respObj->get_response();
    set_output($resp);
  }

  function test_print(){
    $respObj = new Response_api();

    $order_id = "8f3920b5-2bcc-424d-984e-05d90b5efee3";
    $config = $this->config_model->get_config();
    $order = $this->order_model->get_order_by_id($order_id);
    $buyer = $this->user_model->get_user_by_id($order->buyer_id);

    // $pdf=generate_invoice($order, $buyer, $config);
    $data = array(
      "order" => $order,
      "buyer" => $buyer,
      "config" => $config,
    );
    $this->load->view('template/invoice', $data);

    // echo $pdf;
    // $respObj->set_response(200, "success", "Test print success", $pdf);
    // $resp = $respObj->get_response();
    // set_output($resp);
  }

  function test_wa(){
    $phone_number = "628999553686";
    $message = "Test WA send message onesender";
    $resp = send_wa_message_text($phone_number, $message);
    
    $respObj = new Response_api();
    $respObj->set_response(200, "success", "Send WA", $resp);
    $resp = $respObj->get_response();
    set_output($resp);
  }

  function test_snap($token){
    $data['token'] = $token;
    $this->load->view('test/snap', $data);
  }

  function test_file(){


    $dir = "/home/xrilozs/Downloads/WIN/test";

    if (is_dir($dir)) {
      if ($dh = opendir($dir)) {
        $files = [];
        while (($file = readdir($dh)) !== false) {
          array_push($files, $file);
        }
        closedir($dh);

        $i=1;
        // print_r($files);
        foreach ($files as $file) {
          if($file != '.' && $file != '..'){
            rename("$dir/$file","$dir/file$i.txt");
            $i++;
          }
        }
        echo "done";
      }
    }
  }
  
}

?>