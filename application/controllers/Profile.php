<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Profile extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /profile [GET]
    function get_profile(){
        #init req & resp
        $resp_obj = new Response_api();
        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $search = $this->input->get('search');
        $draw = $this->input->get('draw');

        #check token
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/profile [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/profile [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #get profile
        $start = $page_number * $page_size;
        $order = array('field'=>'created_at', 'order'=>'DESC');
        $limit = array('start'=>$start, 'size'=>$page_size);
        $output = array();
        $profile = $this->profile_model->get_profile($search, $order, $limit);
        $total = $this->profile_model->count_profile($search);

        #response
        if(empty($draw)){
            logging('debug', '/profile [GET] - Get profile is success', $profile);
            $resp_obj->set_response(200, "success", "Get profile is success", $profile);
            set_output($resp_obj->get_response());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/profile [GET] - Get profile is success', $profile);
            $resp_obj->set_response_datatable(200, $profile, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        } 
    }

    #path: /profile/all [GET]
    function get_profile_all(){
        #init req & resp
        $resp_obj = new Response_api();
        #get profile
        $profile = $this->profile_model->get_profile();

        #response
        logging('debug', '/profile/all [GET] - Get profile all is success', $profile);
        $resp_obj->set_response(200, "success", "Get profile all is success", $profile);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /profile/by-id/$id [GET]
    function get_profile_by_id($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/profile/by-id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get profile detail
        $profile = $this->profile_model->get_profile_by_id($id);
        if(is_null($profile)){
            logging('error', '/profile/by-id/'.$id.' [GET] - profile not found');
            $resp_obj->set_response(404, "failed", "profile not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/profile/by-id/'.$id.' [GET] - Get profile by id success', $profile);
        $resp_obj->set_response(200, "success", "Get profile by id success", $profile);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /profile/by-alias/$alias [GET]
    function get_profile_by_alias($alias){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #get profile detail
        $profile = $this->profile_model->get_profile_by_alias($alias);
        if(is_null($profile)){
            logging('error', '/profile/by-alias/'.$alias.' [GET] - profile not found');
            $resp_obj->set_response(404, "failed", "profile not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/profile/by-alias/'.$alias.' [GET] - Get profile by alias success', $profile);
        $resp_obj->set_response(200, "success", "Get profile by alias success", $profile);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /profile [POST]
    function create_profile(){
        #init req & res
        $resp_obj = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/profile [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('title', 'content');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/profile [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id'] = get_uniq_id();
        $request['alias'] = generate_alias($request['title']);
    
        #check profile exist
        $profile_exist = $this->profile_model->get_profile_by_alias($request['alias']);
        if($profile_exist){
            logging('error', '/profile [POST] - Requested profile already exist', $request);
            $resp_obj->set_response(400, "failed", "Requested profile already exist");
            set_output($resp_obj->get_response());
            return;
        }

        #create profile
        $flag = $this->profile_model->create_profile($request);
        
        #response
        if(!$flag){
            logging('error', '/profile [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/profile [POST] - Create profile success', $request);
        $resp_obj->set_response(200, "success", "Create profile success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /profile [PUT]
    function update_profile(){
        $resp_obj = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/profile [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'title', 'content');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/profile [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check profile
        $profile = $this->profile_model->get_profile_by_id($request['id']);
        if(is_null($profile)){
            logging('error', '/profile [PUT] - profile not found', $request);
            $resp_obj->set_response(404, "failed", "profile not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update profile
        $request['alias'] = generate_alias($request['title']);
        if($request['alias'] != $profile->alias){
            $profile_exist = $this->profile_model->get_profile_by_alias($request['alias']);
            if($profile_exist){
                logging('error', '/profile [PUT] - profile name duplicate', $request);
                $resp_obj->set_response(400, "failed", "profile name duplicate");
                set_output($resp_obj->get_response());
                return;
            }
        }
        
        $flag = $this->profile_model->update_profile($request);
        #response
        if(empty($flag)){
            logging('error', '/profile [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/profile [PUT] - Update profile success', $request);
        $resp_obj->set_response(200, "success", "Update profile success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /profile/$id [DELETE]
    function delete_profile($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/profile/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check profile
        $profile = $this->profile_model->get_profile_by_id($id);
        if(is_null($profile)){
            logging('error', '/profile/'.$id.' [DELETE] - profile not found');
            $resp_obj->set_response(404, "failed", "profile not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update profile
        $flag = $this->profile_model->delete_profile($id);
        
        #response
        if(empty($flag)){
            logging('error', '/profile/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/profile/'.$id.' [DELETE] - Delete profile success');
        $resp_obj->set_response(200, "success", "Delete profile success");
        set_output($resp_obj->get_response());
        return;
    }
  
}
