<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Video extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /video [GET]
    function get_video(){
        #init req & resp
        $resp_obj           = new Response_api();
        $page_number        = $this->input->get('page_number');
        $page_size          = $this->input->get('page_size');
        $search             = $this->input->get('search');
        $status             = $this->input->get('status');
        $draw               = $this->input->get('draw');
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/video [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #get video
        $start  = $page_number * $page_size;
        $order  = array('field'=>'created_at', 'order'=>'DESC');
        $limit  = array('start'=>$start, 'size'=>$page_size);
        $video   = $this->video_model->get_video($search, $status, $order, $limit);
        $total  = $this->video_model->count_video($search, $status);

        #response
        if(empty($draw)){
            logging('debug', '/video [GET] - Get video is success');
            $resp_obj->set_response(200, "success", "Get video is success", $video);
            set_output($resp_obj->get_response());
            return;
        }else{
            logging('debug', '/video [GET] - Get video is success');
            $resp_obj->set_response_datatable(200, $video, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        } 
    }

    #path: /video/count [GET]
    function count_video(){
        #init req & resp
        $resp_obj           = new Response_api();
        $status             = $this->input->get('status');

        #check token
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/video/count [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #get video
        $total  = $this->video_model->count_video(null, $status);

        #response
        logging('debug', '/video/count [GET] - Count video is success');
        $resp_obj->set_response(200, "success", "Count video is success", $total);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /video/by-id/$id [GET]
    function get_video_by_id($id){
        $resp_obj = new Response_api();

        #get video detail
        $video = $this->video_model->get_video_by_id($id);
        if(is_null($video)){
            logging('error', '/video/by-id/'.$id.' [GET] - video not found');
            $resp_obj->set_response(404, "failed", "video not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/video/by-id/'.$id.' [GET] - Get video by id success');
        $resp_obj->set_response(200, "success", "Get video by id success", $video);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /video/by-alias/$alias [GET]
    function get_video_by_alias($alias){
        $resp_obj = new Response_api();

        #get video detail
        $video = $this->video_model->get_video_by_alias($alias);
        if(is_null($video)){
            logging('error', '/video/by-alias/'.$alias.' [GET] - video not found');
            $resp_obj->set_response(404, "failed", "video not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/video/by-alias/'.$alias.' [GET] - Get video by alias success');
        $resp_obj->set_response(200, "success", "Get video by alias success", $video);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /video [POST]
    function create_video(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/video [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('title', 'description', 'video_url', 'status');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/video [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id'] = get_uniq_id();
        $request['alias'] = generate_alias($request['title']);
        $request['is_youtube'] = 1;
    
        #check video exist
        $video_exist = $this->video_model->get_video_by_alias($request['alias']);
        if($video_exist){
            logging('error', '/video [POST] - Duplicate video title', $request);
            $resp_obj->set_response(400, "failed", "Duplicate video title");
            set_output($resp_obj->get_response());
            return;
        }

        #create video
        $flag = $this->video_model->create_video($request);
        
        #response
        if(!$flag){
            logging('error', '/video [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/video [POST] - Create video success', $request);
        $resp_obj->set_response(200, "success", "Create video success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /video [PUT]
    function update_video(){
        $resp_obj = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/video [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'title', 'video_url', 'description', 'status');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/video [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check video
        $video = $this->video_model->get_video_by_id($request['id']);
        if(is_null($video)){
            logging('error', '/video [PUT] - video not found', $request);
            $resp_obj->set_response(404, "failed", "video not found");
            set_output($resp_obj->get_response());
            return;
        }

        $request['alias'] = generate_alias($request['title']);
        if($request['alias'] != $video->alias){
            $video_exist = $this->video_model->get_video_by_alias($request['alias']);
            if($video_exist){
                logging('error', '/video [PUT] - video name duplicate', $request);
                $resp_obj->set_response(400, "failed", "video name duplicate");
                set_output($resp_obj->get_response());
                return;
            }
        }

        #update video
        $request['is_youtube'] = 1;
        $request['status'] = $video->status;
        $flag = $this->video_model->update_video($request);
        
        #response
        if(empty($flag)){
            logging('error', '/video [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/video [PUT] - Update video success', $request);
        $resp_obj->set_response(200, "success", "Update video success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /video/publish/$id [PUT]
    function publish_video($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/video/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check video
        $video = $this->video_model->get_video_by_id($id);
        if(is_null($video)){
            logging('error', '/video/publish/'.$id.' [PUT] - video not found');
            $resp_obj->set_response(404, "failed", "video not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update video
        $flag = $this->video_model->publish_video($id);
        
        #response
        if(empty($flag)){
            logging('error', '/video/publish/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/video/publish/'.$id.' [PUT] - Publish video success');
        $resp_obj->set_response(200, "success", "Publish video success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /video/unpublish/$id [PUT]
    function unpublish_video($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/video/unpublish/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check video
        $video = $this->video_model->get_video_by_id($id);
        if(is_null($video)){
            logging('error', '/video/unpublish/'.$id.' [PUT] - video not found');
            $resp_obj->set_response(404, "failed", "video not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update video
        $flag = $this->video_model->unpublish_video($id);
        
        #response
        if(empty($flag)){
            logging('error', '/video/unpublish/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/video/unpublish/'.$id.' [PUT] - Unpublish video success');
        $resp_obj->set_response(200, "success", "Unpublish video success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /video/$id [DELETE]
    function delete_video($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/video/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check video
        $video = $this->video_model->get_video_by_id($id);
        if(is_null($video)){
            logging('error', '/video/'.$id.' [DELETE] - video not found');
            $resp_obj->set_response(404, "failed", "video not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update video
        $flag = $this->video_model->delete_video($id);
        
        #response
        if(empty($flag)){
            logging('error', '/video/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/video/'.$id.' [DELETE] - Delete video success');
        $resp_obj->set_response(200, "success", "Delete video success");
        set_output($resp_obj->get_response());
        return;
    }
}
