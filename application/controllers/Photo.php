<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Photo extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /photo [GET]
    function get_photo(){
        #init req & resp
        $resp_obj           = new Response_api();
        $page_number        = $this->input->get('page_number');
        $page_size          = $this->input->get('page_size');
        $search             = $this->input->get('search');
        $status             = $this->input->get('status');
        $draw               = $this->input->get('draw');
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/photo [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #get photo
        $start  = $page_number * $page_size;
        $order  = array('field'=>'created_at', 'order'=>'DESC');
        $limit  = array('start'=>$start, 'size'=>$page_size);
        $photo   = $this->photo_model->get_photo($search, $status, $order, $limit);
        $total  = $this->photo_model->count_photo($search, $status);

        #response
        if(empty($draw)){
            logging('debug', '/photo [GET] - Get photo is success');
            $resp_obj->set_response(200, "success", "Get photo is success", $photo);
            set_output($resp_obj->get_response());
            return;
        }else{
            logging('debug', '/photo [GET] - Get photo is success');
            $resp_obj->set_response_datatable(200, $photo, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        } 
    }

    #path: /photo/count [GET]
    function count_photo(){
        #init req & resp
        $resp_obj           = new Response_api();
        $status             = $this->input->get('status');

        #check token
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/photo/count [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get photo
        $total  = $this->photo_model->count_photo(null, $status);

        #response
        logging('debug', '/photo/count [GET] - Count photo is success');
        $resp_obj->set_response(200, "success", "Count photo is success", $total);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /photo/by-id/$id [GET]
    function get_photo_by_id($id){
        $resp_obj = new Response_api();

        #get photo detail
        $photo = $this->photo_model->get_photo_by_id($id);
        if(is_null($photo)){
            logging('error', '/photo/by-id/'.$id.' [GET] - photo not found');
            $resp_obj->set_response(404, "failed", "photo not found");
            set_output($resp_obj->get_response());
            return;
        }

        $photo_items = $this->photo_item_model->get_photo_item_by_photo_id($id);
        $photo->items = $photo_items;

        #response
        logging('debug', '/photo/by-id/'.$id.' [GET] - Get photo by id success');
        $resp_obj->set_response(200, "success", "Get photo by id success", $photo);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /photo/by-alias/$alias [GET]
    function get_photo_by_alias($alias){
        $resp_obj = new Response_api();

        #get photo detail
        $photo = $this->photo_model->get_photo_by_alias($alias);
        if(is_null($photo)){
            logging('error', '/photo/by-alias/'.$alias.' [GET] - photo not found');
            $resp_obj->set_response(404, "failed", "photo not found");
            set_output($resp_obj->get_response());
            return;
        }

        $photo_items = $this->photo_item_model->get_photo_item_by_photo_id($photo->id);
        $photo->items = $photo_items;

        #response
        logging('debug', '/photo/by-alias/'.$alias.' [GET] - Get photo by alias success');
        $resp_obj->set_response(200, "success", "Get photo by alias success", $photo);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /photo [POST]
    function create_photo(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/photo [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('title', 'description', 'event_date', 'thumbnail_url', 'status');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/photo [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id'] = get_uniq_id();
        $request['alias'] = generate_alias($request['title']);
    
        #check photo exist
        $photo_exist = $this->photo_model->get_photo_by_alias($request['alias']);
        if($photo_exist){
            logging('error', '/photo [POST] - Duplicate photo title', $request);
            $resp_obj->set_response(400, "failed", "Duplicate photo title");
            set_output($resp_obj->get_response());
            return;
        }

        #create photo
        $flag = $this->photo_model->create_photo($request);
        
        #response
        if(!$flag){
            logging('error', '/photo [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/photo [POST] - Create photo success', $request);
        $resp_obj->set_response(200, "success", "Create photo success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /photo [PUT]
    function update_photo(){
        $resp_obj = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/photo [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'title', 'event_date', 'thumbnail_url', 'description');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/photo [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check photo
        $photo = $this->photo_model->get_photo_by_id($request['id']);
        if(is_null($photo)){
            logging('error', '/photo [PUT] - photo not found', $request);
            $resp_obj->set_response(404, "failed", "photo not found");
            set_output($resp_obj->get_response());
            return;
        }

        $request['alias'] = generate_alias($request['title']);
        if($request['alias'] != $photo->alias){
            $photo_exist = $this->photo_model->get_photo_by_alias($request['alias']);
            if($photo_exist){
                logging('error', '/photo [PUT] - photo name duplicate', $request);
                $resp_obj->set_response(400, "failed", "photo name duplicate");
                set_output($resp_obj->get_response());
                return;
            }
        }

        #update photo
        $request['status'] = $photo->status;
        $flag = $this->photo_model->update_photo($request);
        
        #response
        if(empty($flag)){
            logging('error', '/photo [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/photo [PUT] - Update photo success', $request);
        $resp_obj->set_response(200, "success", "Update photo success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /photo/publish/$id [PUT]
    function publish_photo($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/photo/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check photo
        $photo = $this->photo_model->get_photo_by_id($id);
        if(is_null($photo)){
            logging('error', '/photo/publish/'.$id.' [PUT] - photo not found');
            $resp_obj->set_response(404, "failed", "photo not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update photo
        $flag = $this->photo_model->publish_photo($id);
        
        #response
        if(empty($flag)){
            logging('error', '/photo/publish/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/photo/publish/'.$id.' [PUT] - Publish photo success');
        $resp_obj->set_response(200, "success", "Publish photo success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /photo/unpublish/$id [PUT]
    function unpublish_photo($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/photo/unpublish/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check photo
        $photo = $this->photo_model->get_photo_by_id($id);
        if(is_null($photo)){
            logging('error', '/photo/unpublish/'.$id.' [PUT] - photo not found');
            $resp_obj->set_response(404, "failed", "photo not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update photo
        $flag = $this->photo_model->unpublish_photo($id);
        
        #response
        if(empty($flag)){
            logging('error', '/photo/unpublish/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/photo/unpublish/'.$id.' [PUT] - Unpublish photo success');
        $resp_obj->set_response(200, "success", "Unpublish photo success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /photo/$id [DELETE]
    function delete_photo($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/photo/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check photo
        $photo = $this->photo_model->get_photo_by_id($id);
        if(is_null($photo)){
            logging('error', '/photo/'.$id.' [DELETE] - photo not found');
            $resp_obj->set_response(404, "failed", "photo not found");
            set_output($resp_obj->get_response());
            return;
        }

        #delete photo item
        $flag = $this->photo_item_model->delete_photo_item_by_photo_id($id);
        if(empty($flag)){
            logging('error', '/photo/'.$id.' [DELETE] - Internal server error ON DELETE PHOTO ITEM');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }

        #delete photo
        $flag = $this->photo_model->delete_photo($id);
        if(empty($flag)){
            logging('error', '/photo/'.$id.' [DELETE] - Internal server error ON DELETE PHOTO');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/photo/'.$id.' [DELETE] - Delete photo success');
        $resp_obj->set_response(200, "success", "Delete photo success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /photo/upload-image [POST]
    function upload_image(){
        #init variable
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/newphotos/upload-image [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/img/gallery/';
        if (empty($_FILES['image']['name'])) {
            logging('error', '/photo/upload-image [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload image
        $file = $_FILES['image'];
        $resp = upload_image($file, $destination, false);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/photo/upload-image [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }
        $data = $resp['data'];
        $data['full_img_url'] = BASE_URL . $data['img_url'];
        logging('debug', '/photo/upload-image [POST] - Upload image success', $data);
        $resp_obj->set_response(200, "success", "Upload image success", $data);
        set_output($resp_obj->get_response());
        return; 
    }

    #path: /photo/item [POST]
    function add_photo_item(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/photo [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('gallery_photo_id', 'photo_url');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/photo [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }


        #create photo
        $request['id'] = get_uniq_id();
        $flag = $this->photo_item_model->create_photo_item($request);
        
        #response
        if(!$flag){
            logging('error', '/photo [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/photo [POST] - Add photo item success', $request);
        $resp_obj->set_response(200, "success", "Add photo item success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /photo/item/$id [DELETE]
    function remove_photo_item($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/photo/item/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check photo item
        $photo = $this->photo_item_model->get_photo_item_by_id($id);
        if(is_null($photo)){
            logging('error', '/photo/item/'.$id.' [DELETE] - photo not found');
            $resp_obj->set_response(404, "failed", "photo not found");
            set_output($resp_obj->get_response());
            return;
        }

        #delete photo item
        $flag = $this->photo_item_model->delete_photo_item($id);
        if(empty($flag)){
            logging('error', '/photo/item/'.$id.' [DELETE] - Internal server error ON DELETE PHOTO ITEM');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/photo/item/'.$id.' [DELETE] - Delete photo success');
        $resp_obj->set_response(200, "success", "Delete photo success");
        set_output($resp_obj->get_response());
        return;
    }
}
