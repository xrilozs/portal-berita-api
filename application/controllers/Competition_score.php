<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Competition_score extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->helper('url_helper');
  }

  #path: /competition_score/cabang [GET]
  function get_cabang(){
    #init req & resp
    $resp_obj = new Response_api();
    $cabang   = $this->kaltim_mtq_cabang_model->get_active_mtq_cabang();

    logging('debug', '/competition_schedule/active [GET] - Get active cabang is success');
    $resp_obj->set_response(200, "success", "Get active active cabang is success", $cabang);
    set_output($resp_obj->get_response());
    return;
  }

  #path: /competition_score/golongan-by-cabang/$1 [GET]
  function get_golongan_by_cabang($id){
    #init req & resp
    $resp_obj = new Response_api();
    $golongan   = $this->kaltim_mtq_golongan_model->get_mtq_golongan_by_cabang($id);

    logging('debug', '/competition_schedule/golongan-by-cabang/$1 [GET] - Get golongan by cabang is success');
    $resp_obj->set_response(200, "success", "Get golongan by cabang is success", $golongan);
    set_output($resp_obj->get_response());
    return;
  }
}