<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Apps extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /apps [GET]
    function get_apps(){
        #init req & resp
        $resp_obj = new Response_api();
        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $search = $this->input->get('search');
        $draw = $this->input->get('draw');

        #check token
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/apps [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/apps [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #get apps
        $start = $page_number * $page_size;
        $order = array('field'=>'created_at', 'order'=>'DESC');
        $limit = array('start'=>$start, 'size'=>$page_size);
        $output = array();
        $apps = $this->apps_model->get_apps($search, $order, $limit);
        $total = $this->apps_model->count_apps($search);

        #response
        if(empty($draw)){
            logging('debug', '/apps [GET] - Get apps is success', $apps);
            $resp_obj->set_response(200, "success", "Get apps is success", $apps);
            set_output($resp_obj->get_response());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/apps [GET] - Get apps is success', $output);
            $resp_obj->set_response_datatable(200, $apps, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        } 
    }

    #path: /apps/all [GET]
    function get_apps_all(){
        $resp_obj = new Response_api();

        #get apps detail
        $apps = $this->apps_model->get_apps_all();

        #response
        logging('debug', '/apps/all [GET] - Get apps all success', $apps);
        $resp_obj->set_response(200, "success", "Get apps all success", $apps);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /apps/by-id/$id [GET]
    function get_apps_by_id($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/apps/by-id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get apps detail
        $apps = $this->apps_model->get_apps_by_id($id);
        if(is_null($apps)){
            logging('error', '/apps/by-id/'.$id.' [GET] - apps not found');
            $resp_obj->set_response(404, "failed", "apps not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/apps/by-id/'.$id.' [GET] - Get apps by id success', $apps);
        $resp_obj->set_response(200, "success", "Get apps by id success", $apps);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /apps [POST]
    function create_apps(){
        #init req & res
        $resp_obj = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/apps [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('name', 'url', 'icon_url');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/apps [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id'] = get_uniq_id();

        #create apps
        $flag = $this->apps_model->create_apps($request);
        
        #response
        if(!$flag){
            logging('error', '/apps [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/apps [POST] - Create apps success', $request);
        $resp_obj->set_response(200, "success", "Create apps success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /apps [PUT]
    function update_apps(){
        $resp_obj = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/apps [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'name', 'url', 'icon_url');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/apps [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check apps
        $apps = $this->apps_model->get_apps_by_id($request['id']);
        if(is_null($apps)){
            logging('error', '/apps [PUT] - apps not found', $request);
            $resp_obj->set_response(404, "failed", "apps not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update apps
        $flag = $this->apps_model->update_apps($request);
        
        #response
        if(empty($flag)){
            logging('error', '/apps [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/apps [PUT] - Update apps success', $request);
        $resp_obj->set_response(200, "success", "Update apps success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /apps/$id [DELETE]
    function delete_apps($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/apps/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check apps
        $apps = $this->apps_model->get_apps_by_id($id);
        if(is_null($apps)){
            logging('error', '/apps/'.$id.' [DELETE] - apps not found');
            $resp_obj->set_response(404, "failed", "apps not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update apps
        $flag = $this->apps_model->delete_apps($id);
        
        #response
        if(empty($flag)){
            logging('error', '/apps/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/apps/'.$id.' [DELETE] - Delete apps success');
        $resp_obj->set_response(200, "success", "Delete apps success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /apps/upload-icon [POST]
    function upload_icon(){
        #init variable
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/apps/upload-icon [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/img/apps/';
        if (empty($_FILES['icon']['name'])) {
            logging('error', '/apps/upload-icon [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload icon
        $file = $_FILES['icon'];
        $resp = upload_image($file, $destination, false);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/apps/upload-icon [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }
        $data = $resp['data'];
        $data['full_img_url'] = BASE_URL . $data['img_url'];
        logging('debug', '/apps/upload-icon [POST] - Upload apps icon success', $data);
        $resp_obj->set_response(200, "success", "Upload apps icon success", $data);
        set_output($resp_obj->get_response());
        return; 
    }
  
}
