<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Category_news extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /category_news [GET]
    function get_category_news(){
        #init req & resp
        $resp_obj       = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $draw           = $this->input->get('draw');
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/category_news [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #get blog category
        $start          = $page_number * $page_size;
        $order          = array('field'=>'created_at', 'order'=>'DESC');
        $limit          = array('start'=>$start, 'size'=>$page_size);
        $category_news  = $this->category_news_model->get_category_news($search, $order, $limit);
        $total          = $this->category_news_model->count_category_news($search);

        #response
        if(empty($draw)){
            logging('debug', '/category_news [GET] - Get blog category is success');
            $resp_obj->set_response(200, "success", "Get blog category is success", $category_news);
            set_output($resp_obj->get_response());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/category_news [GET] - Get blog category is success');
            $resp_obj->set_response_datatable(200, $category_news, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        } 
    }

    #path: /category_news/all [GET]
    function get_category_news_all(){
        $resp_obj = new Response_api();

        #get blog category detail
        $category_news = $this->category_news_model->get_category_news();

        #response
        logging('debug', '/category_news/all [GET] - Get blog category all success', $category_news);
        $resp_obj->set_response(200, "success", "Get blog category all success", $category_news);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /category_news/by-id/$id [GET]
    function get_category_news_by_id($id){
        $resp_obj = new Response_api();

        #get blog category detail
        $category_news = $this->category_news_model->get_category_news_by_id($id);
        if(is_null($category_news)){
            logging('error', '/category_news/by-id/'.$id.' [GET] - blog category not found');
            $resp_obj->set_response(404, "failed", "blog category not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/category_news/by-id/'.$id.' [GET] - Get blog category by id success', $category_news);
        $resp_obj->set_response(200, "success", "Get blog category by id success", $category_news);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /category_news/by-alias/$alias [GET]
    function get_category_news_by_alias($alias){
        $resp_obj = new Response_api();

        #get blog category detail
        $category_news = $this->category_news_model->get_category_news_by_alias($alias);
        if(is_null($category_news)){
            logging('error', '/category_news/by-alias/'.$alias.' [GET] - blog category not found');
            $resp_obj->set_response(404, "failed", "blog category not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/category_news/by-alias/'.$alias.' [GET] - Get blog category by alias success', $category_news);
        $resp_obj->set_response(200, "success", "Get blog category by alias success", $category_news);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /category_news [POST]
    function create_category_news(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/category_news [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('name');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/category_news [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id']      = get_uniq_id();
        $request['alias']   = generate_alias($request['name']);
    
        #check blog category exist
        $category_news_exist = $this->category_news_model->get_category_news_by_alias($request['alias']);
        if($category_news_exist){
            logging('error', '/category_news [POST] - Duplicate blog category', $request);
            $resp_obj->set_response(400, "failed", "Duplicate blog category");
            set_output($resp_obj->get_response());
            return;
        }

        #create blog category
        $flag = $this->category_news_model->create_category_news($request);
        
        #response
        if(!$flag){
            logging('error', '/category_news [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/category_news [POST] - Create blog category success', $request);
        $resp_obj->set_response(200, "success", "Create blog category success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /category_news [PUT]
    function update_category_news(){
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/category_news [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'name');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/category_news [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check blog category
        $category_news = $this->category_news_model->get_category_news_by_id($request['id']);
        if(is_null($category_news)){
            logging('error', '/category_news [PUT] - blog category not found', $request);
            $resp_obj->set_response(404, "failed", "blog category not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update blog category
        $request['alias']       = generate_alias($request['name']);

        if($request['alias'] != $category_news->alias){
            $category_news_exist = $this->category_news_model->get_category_news_by_alias($request['alias']);
            if($category_news_exist){
                logging('error', '/category_news [PUT] - blog category name duplicate', $request);
                $resp_obj->set_response(400, "failed", "blog category name duplicate");
                set_output($resp_obj->get_response());
                return;
            }
        }
        $flag = $this->category_news_model->update_category_news($request);
        
        #response
        if(empty($flag)){
            logging('error', '/category_news [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/category_news [PUT] - Update blog category success', $request);
        $resp_obj->set_response(200, "success", "Update blog category success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /category_news/$id [DELETE] 
    function delete_category_news($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/category_news/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check blog category
        $category_news = $this->category_news_model->get_category_news_by_id($id);
        if(is_null($category_news)){
            logging('error', '/category_news/'.$id.' [DELETE] - blog category not found');
            $resp_obj->set_response(404, "failed", "blog category not found");
            set_output($resp_obj->get_response());
            return;
        }

        #delete blog category
        $flag = $this->category_news_model->delete_category_news($id);
        
        #response
        if(!$flag){
            logging('error', '/category_news/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/category_news/'.$id.' [DELETE] - Delete blog category success');
        $resp_obj->set_response(200, "success", "Delete blog category success", $flag);
        set_output($resp_obj->get_response());
        return;
    }
  
}
