<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Document extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /document [GET]
    function get_document(){
        #init req & resp
        $resp_obj           = new Response_api();
        $page_number        = $this->input->get('page_number');
        $page_size          = $this->input->get('page_size');
        $search             = $this->input->get('search');
        $status             = $this->input->get('status');
        $draw               = $this->input->get('draw');
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/document [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #get document
        $start  = $page_number * $page_size;
        $order  = array('field'=>'created_at', 'order'=>'DESC');
        $limit  = array('start'=>$start, 'size'=>$page_size);
        $document   = $this->document_model->get_document($search, $status, $order, $limit);
        $total  = $this->document_model->count_document($search, $status);

        #response
        if(empty($draw)){
            logging('debug', '/document [GET] - Get document is success');
            $resp_obj->set_response(200, "success", "Get document is success", $document);
            set_output($resp_obj->get_response());
            return;
        }else{
            logging('debug', '/document [GET] - Get document is success');
            $resp_obj->set_response_datatable(200, $document, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        } 
    }

    #path: /document/count [GET]
    function count_document(){
        #init req & resp
        $resp_obj           = new Response_api();
        $status             = $this->input->get('status');

        #check token
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/document/count [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get document
        $total  = $this->document_model->count_document(null, $status);

        #response
        logging('debug', '/document [GET] - Count document is success');
        $resp_obj->set_response(200, "success", "Count document is success", $total);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /document/by-id/$id [GET]
    function get_document_by_id($id){
        $resp_obj = new Response_api();

        #get document detail
        $document = $this->document_model->get_document_by_id($id);
        if(is_null($document)){
            logging('error', '/document/by-id/'.$id.' [GET] - document not found');
            $resp_obj->set_response(404, "failed", "document not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/document/by-id/'.$id.' [GET] - Get document by id success');
        $resp_obj->set_response(200, "success", "Get document by id success", $document);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /document/by-alias/$alias [GET]
    function get_document_by_alias($alias){
        $resp_obj = new Response_api();

        #get document detail
        $document = $this->document_model->get_document_by_alias($alias);
        if(is_null($document)){
            logging('error', '/document/by-alias/'.$alias.' [GET] - document not found');
            $resp_obj->set_response(404, "failed", "document not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/document/by-alias/'.$alias.' [GET] - Get document by alias success');
        $resp_obj->set_response(200, "success", "Get document by alias success", $document);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /document [POST]
    function create_document(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/document [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('name', 'size', 'document_url', 'status');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/document [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id'] = get_uniq_id();
        $request['alias'] = generate_alias($request['name']);
    
        #check document exist
        $document_exist = $this->document_model->get_document_by_alias($request['alias']);
        if($document_exist){
            logging('error', '/document [POST] - Duplicate document name', $request);
            $resp_obj->set_response(400, "failed", "Duplicate document name");
            set_output($resp_obj->get_response());
            return;
        }

        #create document
        $flag = $this->document_model->create_document($request);
        
        #response
        if(!$flag){
            logging('error', '/document [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/document [POST] - Create document success', $request);
        $resp_obj->set_response(200, "success", "Create document success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /document [PUT]
    function update_document(){
        $resp_obj = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/document [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'name', 'document_url', 'size');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/document [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check document
        $document = $this->document_model->get_document_by_id($request['id']);
        if(is_null($document)){
            logging('error', '/document [PUT] - document not found', $request);
            $resp_obj->set_response(404, "failed", "document not found");
            set_output($resp_obj->get_response());
            return;
        }

        $request['alias'] = generate_alias($request['name']);
        if($request['alias'] != $document->alias){
            $document_exist = $this->document_model->get_document_by_alias($request['alias']);
            if($document_exist){
                logging('error', '/document [PUT] - document name duplicate', $request);
                $resp_obj->set_response(400, "failed", "document name duplicate");
                set_output($resp_obj->get_response());
                return;
            }
        }

        #update document
        $request['status'] = $document->status;
        $flag = $this->document_model->update_document($request);
        
        #response
        if(empty($flag)){
            logging('error', '/document [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/document [PUT] - Update document success', $request);
        $resp_obj->set_response(200, "success", "Update document success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /document/publish/$id [PUT]
    function publish_document($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/document/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check document
        $document = $this->document_model->get_document_by_id($id);
        if(is_null($document)){
            logging('error', '/document/publish/'.$id.' [PUT] - document not found');
            $resp_obj->set_response(404, "failed", "document not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update document
        $flag = $this->document_model->publish_document($id);
        
        #response
        if(empty($flag)){
            logging('error', '/document/publish/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/document/publish/'.$id.' [PUT] - Publish document success');
        $resp_obj->set_response(200, "success", "Publish document success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /document/unpublish/$id [PUT]
    function unpublish_document($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/document/unpublish/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check document
        $document = $this->document_model->get_document_by_id($id);
        if(is_null($document)){
            logging('error', '/document/unpublish/'.$id.' [PUT] - document not found');
            $resp_obj->set_response(404, "failed", "document not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update document
        $flag = $this->document_model->unpublish_document($id);
        
        #response
        if(empty($flag)){
            logging('error', '/document/unpublish/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/document/unpublish/'.$id.' [PUT] - Unpublish document success');
        $resp_obj->set_response(200, "success", "Unpublish document success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /document/$id [DELETE]
    function delete_document($id){
        $resp_obj = new Response_api();
        $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header, $allowed_role);
        if($resp['status'] == 'failed'){
            logging('error', '/document/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check document
        $document = $this->document_model->get_document_by_id($id);
        if(is_null($document)){
            logging('error', '/document/'.$id.' [DELETE] - document not found');
            $resp_obj->set_response(404, "failed", "document not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update document
        $flag = $this->document_model->delete_document($id);
        
        #response
        if(empty($flag)){
            logging('error', '/document/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/document/'.$id.' [DELETE] - Delete document success');
        $resp_obj->set_response(200, "success", "Delete document success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /document/upload-document [POST]
    function upload_document(){
        #init variable
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/document/upload-document [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/document/';
        if (empty($_FILES['doc']['name'])) {
            logging('error', '/document/upload-document [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload document
        $file = $_FILES['doc'];
        $resp = upload_file($file, $destination, 500097152);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/document/upload-document [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }

        $data['size']               = convertByte($file['size']);
        $data['document_url']       = $resp['data'];
        $data['full_document_url']  = BASE_URL . $resp['data'];
        logging('debug', '/document/upload-document [POST] - Upload document success', $data);
        $resp_obj->set_response(200, "success", "Upload document success", $data);
        set_output($resp_obj->get_response());
        return; 
    }
}
