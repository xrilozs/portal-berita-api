<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Config extends CI_Controller {
  public function __construct($config = 'rest'){
    parent::__construct($config);
  }
  
  #path: /config [GET]
  function get_config(){
    $resp = new Response_api();

    #get config
    $config = $this->config_model->get_config();
    
    #response
    logging('debug', '/config [GET] - Get config success', $config);
    $resp->set_response(200, "success", "Get config success", $config);
    set_output($resp->get_response());
    return;
  }
  
  #path: /config/contact [PUT]
  function update_contact(){
    $resp = new Response_api();
    $request = json_decode($this->input->raw_input_stream, true);
    $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

    #check token
    $header = $this->input->request_headers();
    $resp_token = verify_admin_token($header, $allowed_role);
    if($resp_token['status'] == 'failed'){
      logging('error', '/config/contact [PUT] - '.$resp_token['message']);
      set_output($resp_token);
      return;
    }

    if(array_key_exists('id', $request)){
      #check config
      $config = $this->config_model->get_config_by_id($request['id']);
      if(is_null($config)){
        logging('error', '/config/contact [PUT] - config not found', $request);
        $resp->set_response(404, "failed", "config not found");
        set_output($resp->get_response());
        return;
      }
      $data = (array) $config;
      unset($data["updated_at"]);
    }else{
      $data['id']         = get_uniq_id();
      $data['created_at'] = date('Y-m-d H:i:s');
    }

    $data['address']    = $request['address'];
    $data['phone']      = $request['phone'];
    $data['email']      = $request['email'];
    $data['google_map'] = $request['google_map'];

    #update config
    $flag = $this->config_model->create_update_config($data);
    logging('debug', '/config/contact [PUT] - Update contact success', $request);
    $resp->set_response(200, "success", "Update contact success", $request);
    set_output($resp->get_response());
    return;
  }

  #path: /config/social [PUT]
  function update_social(){
    $resp = new Response_api();
    $request = json_decode($this->input->raw_input_stream, true);
    $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

    #check token
    $header = $this->input->request_headers();
    $resp_token = verify_admin_token($header, $allowed_role);
    if($resp_token['status'] == 'failed'){
      logging('error', '/config/social [PUT] - '.$resp_token['message']);
      set_output($resp_token);
      return;
    }

    if(array_key_exists('id', $request)){
      #check config
      $config = $this->config_model->get_config_by_id($request['id']);
      if(is_null($config)){
        logging('error', '/config/social [PUT] - config not found', $request);
        $resp->set_response(404, "failed", "config not found");
        set_output($resp->get_response());
        return;
      }
      $data = (array) $config;
      unset($data["updated_at"]);
    }else{
      $data['id']         = get_uniq_id();
      $data['created_at'] = date('Y-m-d H:i:s');
    }

    $data['sm_twitter']   = $request['sm_twitter'];
    $data['sm_instagram'] = $request['sm_instagram'];
    $data['sm_facebook']  = $request['sm_facebook'];
    $data['sm_youtube']   = $request['sm_youtube'];

    #update config
    $flag = $this->config_model->create_update_config($data);
    logging('debug', '/config/social [PUT] - Update social success', $data);
    $resp->set_response(200, "success", "Update web social success", $data);
    set_output($resp->get_response());
    return;
  }

  #path: /config/web [PUT]
  function update_web(){
    $resp = new Response_api();
    $request = json_decode($this->input->raw_input_stream, true);
    $allowed_role = array('SUPERADMIN', 'ADMIN', 'USER');

    #check token
    $header = $this->input->request_headers();
    $resp_token = verify_admin_token($header, $allowed_role);
    if($resp_token['status'] == 'failed'){
      logging('error', '/config/web [PUT] - '.$resp_token['message']);
      set_output($resp_token);
      return;
    }

    if(array_key_exists('id', $request)){
      #check config
      $config = $this->config_model->get_config_by_id($request['id']);
      if(is_null($config)){
        logging('error', '/config/web [PUT] - config not found', $request);
        $resp->set_response(404, "failed", "config not found");
        set_output($resp->get_response());
        return;
      }
      $data = (array) $config;
      unset($data["updated_at"]);
    }else{
      $data['id']         = get_uniq_id();
      $data['created_at'] = date('Y-m-d H:i:s');
    }

    $data['logo_1_url']  = $request['logo_1_url'];
    $data['logo_2_url']  = $request['logo_2_url'];
    $data['title_1']     = $request['title_1'];
    $data['title_2']     = $request['title_2'];
    $data['icon_url']    = $request['icon_url'];

    #update config
    $flag = $this->config_model->create_update_config($data);
    logging('debug', '/config/web [PUT] - Update web success', $request);
    $resp->set_response(200, "success", "Update web success", $request);
    set_output($resp->get_response());
    return;
  }
  
  #path: /config/upload-image [PUT]
  function upload_image(){
    #init variable
    $resp_obj = new Response_api();

    #check token
    $header = $this->input->request_headers();
    $resp = verify_admin_token($header);
    if($resp['status'] == 'failed'){
        logging('error', '/config/upload-image [POST] - '.$resp['message']);
        set_output($resp);
        return;
    }

    #check requested param
    $destination = 'assets/img/web/';
    if (empty($_FILES['image']['name'])) {
        logging('error', '/config/upload-image [POST] - Missing parameter. please check API documentation');
        $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
        set_output($resp_obj->get_response());
        return;   
    }

    #upload image
    $file = $_FILES['image'];
    $resp = upload_image($file, $destination, false);

    #response
    if($resp['status'] == 'failed'){
        logging('error', '/config/upload-image [POST] - '.$resp['message']);
        $resp_obj->set_response(400, "failed", $resp['message']);
        set_output($resp_obj->get_response());
        return; 
    }
    $data = $resp['data'];
    $data['full_img_url'] = BASE_URL . $data['img_url'];
    logging('debug', '/config/upload-image [POST] - Upload image success', $data);
    $resp_obj->set_response(200, "success", "Upload image success", $data);
    set_output($resp_obj->get_response());
    return; 
  }
}

?>